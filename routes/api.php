<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('admin/collaborations', 'CollaborationAPIController@index');
Route::post('admin/collaborations', 'CollaborationAPIController@store');
Route::get('admin/collaborations/{collaborations}', 'CollaborationAPIController@show');
Route::put('admin/collaborations/{collaborations}', 'CollaborationAPIController@update');
Route::patch('admin/collaborations/{collaborations}', 'CollaborationAPIController@update');
Route::delete('admin/collaborations{collaborations}', 'CollaborationAPIController@destroy');

Route::get('admin/venues', 'VenueAPIController@index');
Route::post('admin/venues', 'VenueAPIController@store');
Route::get('admin/venues/{venues}', 'VenueAPIController@show');
Route::put('admin/venues/{venues}', 'VenueAPIController@update');
Route::patch('admin/venues/{venues}', 'VenueAPIController@update');
Route::delete('admin/venues{venues}', 'VenueAPIController@destroy');

Route::get('admin/events', 'EventAPIController@index');
Route::post('admin/events', 'EventAPIController@store');
Route::get('admin/events/{events}', 'EventAPIController@show');
Route::put('admin/events/{events}', 'EventAPIController@update');
Route::patch('admin/events/{events}', 'EventAPIController@update');
Route::delete('admin/events{events}', 'EventAPIController@destroy');

Route::get('admin/job_positions', 'JobPositionAPIController@index');
Route::post('admin/job_positions', 'JobPositionAPIController@store');
Route::get('admin/job_positions/{job_positions}', 'JobPositionAPIController@show');
Route::put('admin/job_positions/{job_positions}', 'JobPositionAPIController@update');
Route::patch('admin/job_positions/{job_positions}', 'JobPositionAPIController@update');
Route::delete('admin/job_positions{job_positions}', 'JobPositionAPIController@destroy');

Route::get('admin/business_sectors', 'BusinessSectorAPIController@index');
Route::post('admin/business_sectors', 'BusinessSectorAPIController@store');
Route::get('admin/business_sectors/{business_sectors}', 'BusinessSectorAPIController@show');
Route::put('admin/business_sectors/{business_sectors}', 'BusinessSectorAPIController@update');
Route::patch('admin/business_sectors/{business_sectors}', 'BusinessSectorAPIController@update');
Route::delete('admin/business_sectors{business_sectors}', 'BusinessSectorAPIController@destroy');

Route::get('admin/attendees', 'AttendeeAPIController@index');
Route::post('admin/attendees', 'AttendeeAPIController@store');
Route::get('admin/attendees/{attendees}', 'AttendeeAPIController@show');
Route::put('admin/attendees/{attendees}', 'AttendeeAPIController@update');
Route::patch('admin/attendees/{attendees}', 'AttendeeAPIController@update');
Route::delete('admin/attendees{attendees}', 'AttendeeAPIController@destroy');

Route::get('admin/pictures', 'PictureAPIController@index');
Route::post('admin/pictures', 'PictureAPIController@store');
Route::get('admin/pictures/{pictures}', 'PictureAPIController@show');
Route::put('admin/pictures/{pictures}', 'PictureAPIController@update');
Route::patch('admin/pictures/{pictures}', 'PictureAPIController@update');
Route::delete('admin/pictures{pictures}', 'PictureAPIController@destroy');

Route::get('admin/meetings', 'MeetingAPIController@index');
Route::post('admin/meetings', 'MeetingAPIController@store');
Route::get('admin/meetings/{meetings}', 'MeetingAPIController@show');
Route::put('admin/meetings/{meetings}', 'MeetingAPIController@update');
Route::patch('admin/meetings/{meetings}', 'MeetingAPIController@update');
Route::delete('admin/meetings{meetings}', 'MeetingAPIController@destroy');

Route::get('admin/meeting_statuses', 'MeetingStatusAPIController@index');
Route::post('admin/meeting_statuses', 'MeetingStatusAPIController@store');
Route::get('admin/meeting_statuses/{meeting_statuses}', 'MeetingStatusAPIController@show');
Route::put('admin/meeting_statuses/{meeting_statuses}', 'MeetingStatusAPIController@update');
Route::patch('admin/meeting_statuses/{meeting_statuses}', 'MeetingStatusAPIController@update');
Route::delete('admin/meeting_statuses{meeting_statuses}', 'MeetingStatusAPIController@destroy');