<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Support\Facades\Input;
//use League\Glide\Responses\LaravelResponseFactory;
//use League\Glide\ServerFactory;

Route::get('/', 'FrontPageController@homepage');
Route::get('/about', 'FrontPageController@aboutUs');
Route::get('/events', 'FrontPageController@events');
Route::get('/event/{id}', 'FrontPageController@event_detail')->name('frontend.event_detail');

Route::get('/registration', 'FrontPageController@registration');
Route::post('/registration', 'RegistrationController@store');

Auth::routes();

Route::get('/home', 'HomeController@index');

//
//Route::get('glide/{path}', function ($path) {
//	$server = ServerFactory::create([
//		'source'   => app('filesystem')->disk('public_images')->getDriver(),
//		'cache'    => storage_path('glide'),
//		'response' => new LaravelResponseFactory()
//	]);
//
//	return $server->getImageResponse($path, Input::query());
//})->where('path', '.+');

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:admin'], 'as' => 'admin.'], function () {
	Route::resource('collaborations', 'CollaborationController');
	Route::resource('venues', 'VenueController');
	Route::resource('events', 'EventController');
	Route::resource('events/{event}/pictures', 'PictureController');
	Route::get('events/{event}/registered_users', ['as' => 'events.registered_users', 'uses' => 'EventController@registeredUsers']);
	Route::get('events/{event}/meeting/{users}/', ['as' => 'events.arrange_meeting', 'uses' => 'EventController@arrange_meeting']);
	Route::post('events/{event}/meeting/{users}/', ['as' => 'events.arrange_meeting.store', 'uses' => 'EventController@arrange_meetingPost']);
	Route::resource('jobPositions', 'JobPositionController');
	Route::resource('businessSectors', 'BusinessSectorController');
	Route::resource('attendees', 'AttendeeController');
});

Route::group(['prefix' => 'attendee', 'middleware' => ['auth', 'role:attendee'], 'as' => 'attendee.'], function () {
	Route::get('events_for_attendees', ['as' => 'events_for_attendees', 'uses' => 'EventsForAttendeesController@events_list']);
	Route::get('events_for_attendees/register/{event_id}', ['as' => 'events_for_attendees.register', 'uses' => 'EventsForAttendeesController@registerForEvent']);
	Route::post('events_for_attendees/registerInterest/{event_id}', ['as' => 'events_for_attendees.registerInterest', 'uses' => 'EventsForAttendeesController@registerEventInterest']);
	Route::get('events_for_attendees/register_events', ['as' => 'events_for_attendees.registered_events', 'uses' => 'EventsForAttendeesController@registeredEvents']);
});

//Route::get('admin/collaborations', ['as' => 'admin.collaborations.index', 'uses' => 'CollaborationController@index']);
//Route::post('admin/collaborations', ['as' => 'admin.collaborations.store', 'uses' => 'CollaborationController@store']);
//Route::get('admin/collaborations/create', ['as' => 'admin.collaborations.create', 'uses' => 'CollaborationController@create']);
//Route::put('admin/collaborations/{collaborations}', ['as' => 'admin.collaborations.update', 'uses' => 'CollaborationController@update']);
//Route::patch('admin/collaborations/{collaborations}', ['as' => 'admin.collaborations.update', 'uses' => 'CollaborationController@update']);
//Route::delete('admin/collaborations/{collaborations}', ['as' => 'admin.collaborations.destroy', 'uses' => 'CollaborationController@destroy']);
//Route::get('admin/collaborations/{collaborations}', ['as' => 'admin.collaborations.show', 'uses' => 'CollaborationController@show']);
//Route::get('admin/collaborations/{collaborations}/edit', ['as' => 'admin.collaborations.edit', 'uses' => 'CollaborationController@edit']);
//
//
//Route::get('admin/venues', ['as' => 'admin.venues.index', 'uses' => 'VenueController@index']);
//Route::post('admin/venues', ['as' => 'admin.venues.store', 'uses' => 'VenueController@store']);
//Route::get('admin/venues/create', ['as' => 'admin.venues.create', 'uses' => 'VenueController@create']);
//Route::put('admin/venues/{venues}', ['as' => 'admin.venues.update', 'uses' => 'VenueController@update']);
//Route::patch('admin/venues/{venues}', ['as' => 'admin.venues.update', 'uses' => 'VenueController@update']);
//Route::delete('admin/venues/{venues}', ['as' => 'admin.venues.destroy', 'uses' => 'VenueController@destroy']);
//Route::get('admin/venues/{venues}', ['as' => 'admin.venues.show', 'uses' => 'VenueController@show']);
//Route::get('admin/venues/{venues}/edit', ['as' => 'admin.venues.edit', 'uses' => 'VenueController@edit']);
//
//
//Route::get('admin/events', ['as' => 'admin.events.index', 'uses' => 'EventController@index']);
//Route::post('admin/events', ['as' => 'admin.events.store', 'uses' => 'EventController@store']);
//Route::get('admin/events/create', ['as' => 'admin.events.create', 'uses' => 'EventController@create']);
//Route::put('admin/events/{events}', ['as' => 'admin.events.update', 'uses' => 'EventController@update']);
//Route::patch('admin/events/{events}', ['as' => 'admin.events.update', 'uses' => 'EventController@update']);
//Route::delete('admin/events/{events}', ['as' => 'admin.events.destroy', 'uses' => 'EventController@destroy']);
//Route::get('admin/events/{events}', ['as' => 'admin.events.show', 'uses' => 'EventController@show']);
//Route::get('admin/events/{events}/edit', ['as' => 'admin.events.edit', 'uses' => 'EventController@edit']);
//
//
//Route::get('admin/jobPositions', ['as' => 'admin.jobPositions.index', 'uses' => 'JobPositionController@index']);
//Route::post('admin/jobPositions', ['as' => 'admin.jobPositions.store', 'uses' => 'JobPositionController@store']);
//Route::get('admin/jobPositions/create', ['as' => 'admin.jobPositions.create', 'uses' => 'JobPositionController@create']);
//Route::put('admin/jobPositions/{jobPositions}', ['as' => 'admin.jobPositions.update', 'uses' => 'JobPositionController@update']);
//Route::patch('admin/jobPositions/{jobPositions}', ['as' => 'admin.jobPositions.update', 'uses' => 'JobPositionController@update']);
//Route::delete('admin/jobPositions/{jobPositions}', ['as' => 'admin.jobPositions.destroy', 'uses' => 'JobPositionController@destroy']);
//Route::get('admin/jobPositions/{jobPositions}', ['as' => 'admin.jobPositions.show', 'uses' => 'JobPositionController@show']);
//Route::get('admin/jobPositions/{jobPositions}/edit', ['as' => 'admin.jobPositions.edit', 'uses' => 'JobPositionController@edit']);
//
//
//Route::get('admin/businessSectors', ['as' => 'admin.businessSectors.index', 'uses' => 'BusinessSectorController@index']);
//Route::post('admin/businessSectors', ['as' => 'admin.businessSectors.store', 'uses' => 'BusinessSectorController@store']);
//Route::get('admin/businessSectors/create', ['as' => 'admin.businessSectors.create', 'uses' => 'BusinessSectorController@create']);
//Route::put('admin/businessSectors/{businessSectors}', ['as' => 'admin.businessSectors.update', 'uses' => 'BusinessSectorController@update']);
//Route::patch('admin/businessSectors/{businessSectors}', ['as' => 'admin.businessSectors.update', 'uses' => 'BusinessSectorController@update']);
//Route::delete('admin/businessSectors/{businessSectors}', ['as' => 'admin.businessSectors.destroy', 'uses' => 'BusinessSectorController@destroy']);
//Route::get('admin/businessSectors/{businessSectors}', ['as' => 'admin.businessSectors.show', 'uses' => 'BusinessSectorController@show']);
//Route::get('admin/businessSectors/{businessSectors}/edit', ['as' => 'admin.businessSectors.edit', 'uses' => 'BusinessSectorController@edit']);
//
//
//Route::get('admin/attendees', ['as' => 'admin.attendees.index', 'uses' => 'AttendeeController@index']);
//Route::post('admin/attendees', ['as' => 'admin.attendees.store', 'uses' => 'AttendeeController@store']);
//Route::get('admin/attendees/create', ['as' => 'admin.attendees.create', 'uses' => 'AttendeeController@create']);
//Route::put('admin/attendees/{attendees}', ['as' => 'admin.attendees.update', 'uses' => 'AttendeeController@update']);
//Route::patch('admin/attendees/{attendees}', ['as' => 'admin.attendees.update', 'uses' => 'AttendeeController@update']);
//Route::delete('admin/attendees/{attendees}', ['as' => 'admin.attendees.destroy', 'uses' => 'AttendeeController@destroy']);
//Route::get('admin/attendees/{attendees}', ['as' => 'admin.attendees.show', 'uses' => 'AttendeeController@show']);
//Route::get('admin/attendees/{attendees}/edit', ['as' => 'admin.attendees.edit', 'uses' => 'AttendeeController@edit']);


//Route::get('admin/pictures', ['as' => 'admin.pictures.index', 'uses' => 'PictureController@index']);
//Route::post('admin/pictures', ['as' => 'admin.pictures.store', 'uses' => 'PictureController@store']);
//Route::get('admin/pictures/create', ['as' => 'admin.pictures.create', 'uses' => 'PictureController@create']);
//Route::put('admin/pictures/{pictures}', ['as' => 'admin.pictures.update', 'uses' => 'PictureController@update']);
//Route::patch('admin/pictures/{pictures}', ['as' => 'admin.pictures.update', 'uses' => 'PictureController@update']);
//Route::delete('admin/pictures/{pictures}', ['as' => 'admin.pictures.destroy', 'uses' => 'PictureController@destroy']);
//Route::get('admin/pictures/{pictures}', ['as' => 'admin.pictures.show', 'uses' => 'PictureController@show']);
//Route::get('admin/pictures/{pictures}/edit', ['as' => 'admin.pictures.edit', 'uses' => 'PictureController@edit']);


Route::get('admin/meetings', ['as'=> 'admin.meetings.index', 'uses' => 'MeetingController@index']);
Route::post('admin/meetings', ['as'=> 'admin.meetings.store', 'uses' => 'MeetingController@store']);
Route::get('admin/meetings/create', ['as'=> 'admin.meetings.create', 'uses' => 'MeetingController@create']);
Route::put('admin/meetings/{meetings}', ['as'=> 'admin.meetings.update', 'uses' => 'MeetingController@update']);
Route::patch('admin/meetings/{meetings}', ['as'=> 'admin.meetings.update', 'uses' => 'MeetingController@update']);
Route::delete('admin/meetings/{meetings}', ['as'=> 'admin.meetings.destroy', 'uses' => 'MeetingController@destroy']);
Route::get('admin/meetings/{meetings}', ['as'=> 'admin.meetings.show', 'uses' => 'MeetingController@show']);
Route::get('admin/meetings/{meetings}/edit', ['as'=> 'admin.meetings.edit', 'uses' => 'MeetingController@edit']);


Route::get('admin/meetingStatuses', ['as'=> 'admin.meetingStatuses.index', 'uses' => 'MeetingStatusController@index']);
Route::post('admin/meetingStatuses', ['as'=> 'admin.meetingStatuses.store', 'uses' => 'MeetingStatusController@store']);
Route::get('admin/meetingStatuses/create', ['as'=> 'admin.meetingStatuses.create', 'uses' => 'MeetingStatusController@create']);
Route::put('admin/meetingStatuses/{meetingStatuses}', ['as'=> 'admin.meetingStatuses.update', 'uses' => 'MeetingStatusController@update']);
Route::patch('admin/meetingStatuses/{meetingStatuses}', ['as'=> 'admin.meetingStatuses.update', 'uses' => 'MeetingStatusController@update']);
Route::delete('admin/meetingStatuses/{meetingStatuses}', ['as'=> 'admin.meetingStatuses.destroy', 'uses' => 'MeetingStatusController@destroy']);
Route::get('admin/meetingStatuses/{meetingStatuses}', ['as'=> 'admin.meetingStatuses.show', 'uses' => 'MeetingStatusController@show']);
Route::get('admin/meetingStatuses/{meetingStatuses}/edit', ['as'=> 'admin.meetingStatuses.edit', 'uses' => 'MeetingStatusController@edit']);
