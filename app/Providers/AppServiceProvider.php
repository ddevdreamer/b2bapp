<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Schema::defaultStringLength(191);
		//compose all the views....
		view()->composer('*', function ($view) {
			$user = auth()->user();
			$signedIn = auth()->check();
			//...with this variable
			$view->with('currentUser', $user);
			$view->with('signedIn', $signedIn);
		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
