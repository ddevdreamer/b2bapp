<?php

namespace App\Http\Controllers;

use App\DataTables\MeetingDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMeetingRequest;
use App\Http\Requests\UpdateMeetingRequest;
use App\Repositories\MeetingRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MeetingController extends AppBaseController
{
    /** @var  MeetingRepository */
    private $meetingRepository;

    public function __construct(MeetingRepository $meetingRepo)
    {
        $this->meetingRepository = $meetingRepo;
    }

    /**
     * Display a listing of the Meeting.
     *
     * @param MeetingDataTable $meetingDataTable
     * @return Response
     */
    public function index(MeetingDataTable $meetingDataTable)
    {
        return $meetingDataTable->render('backend.meetings.index');
    }

    /**
     * Show the form for creating a new Meeting.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.meetings.create');
    }

    /**
     * Store a newly created Meeting in storage.
     *
     * @param CreateMeetingRequest $request
     *
     * @return Response
     */
    public function store(CreateMeetingRequest $request)
    {
        $input = $request->all();

        $meeting = $this->meetingRepository->create($input);

        Flash::success('Meeting saved successfully.');

        return redirect(route('admin.meetings.index'));
    }

    /**
     * Display the specified Meeting.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $meeting = $this->meetingRepository->findWithoutFail($id);

        if (empty($meeting)) {
            Flash::error('Meeting not found');

            return redirect(route('admin.meetings.index'));
        }

        return view('backend.meetings.show')->with('meeting', $meeting);
    }

    /**
     * Show the form for editing the specified Meeting.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $meeting = $this->meetingRepository->findWithoutFail($id);

        if (empty($meeting)) {
            Flash::error('Meeting not found');

            return redirect(route('admin.meetings.index'));
        }

        return view('backend.meetings.edit')->with('meeting', $meeting);
    }

    /**
     * Update the specified Meeting in storage.
     *
     * @param  int              $id
     * @param UpdateMeetingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMeetingRequest $request)
    {
        $meeting = $this->meetingRepository->findWithoutFail($id);

        if (empty($meeting)) {
            Flash::error('Meeting not found');

            return redirect(route('admin.meetings.index'));
        }

        $meeting = $this->meetingRepository->update($request->all(), $id);

        Flash::success('Meeting updated successfully.');

        return redirect(route('admin.meetings.index'));
    }

    /**
     * Remove the specified Meeting from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $meeting = $this->meetingRepository->findWithoutFail($id);

        if (empty($meeting)) {
            Flash::error('Meeting not found');

            return redirect(route('admin.meetings.index'));
        }

        $this->meetingRepository->delete($id);

        Flash::success('Meeting deleted successfully.');

        return redirect(route('admin.meetings.index'));
    }
}
