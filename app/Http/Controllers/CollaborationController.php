<?php

namespace App\Http\Controllers;

use App\DataTables\CollaborationDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCollaborationRequest;
use App\Http\Requests\UpdateCollaborationRequest;
use App\Repositories\CollaborationRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CollaborationController extends AppBaseController
{
    /** @var  CollaborationRepository */
    private $collaborationRepository;

    public function __construct(CollaborationRepository $collaborationRepo)
    {
        $this->collaborationRepository = $collaborationRepo;
    }

    /**
     * Display a listing of the Collaboration.
     *
     * @param CollaborationDataTable $collaborationDataTable
     * @return Response
     */
    public function index(CollaborationDataTable $collaborationDataTable)
    {
        return $collaborationDataTable->render('backend.collaborations.index');
    }

    /**
     * Show the form for creating a new Collaboration.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.collaborations.create');
    }

    /**
     * Store a newly created Collaboration in storage.
     *
     * @param CreateCollaborationRequest $request
     *
     * @return Response
     */
    public function store(CreateCollaborationRequest $request)
    {
        $input = $request->all();

        $collaboration = $this->collaborationRepository->create($input);

        Flash::success('Collaboration saved successfully.');

        return redirect(route('admin.collaborations.index'));
    }

    /**
     * Display the specified Collaboration.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $collaboration = $this->collaborationRepository->findWithoutFail($id);

        if (empty($collaboration)) {
            Flash::error('Collaboration not found');

            return redirect(route('admin.collaborations.index'));
        }

        return view('backend.collaborations.show')->with('collaboration', $collaboration);
    }

    /**
     * Show the form for editing the specified Collaboration.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $collaboration = $this->collaborationRepository->findWithoutFail($id);

        if (empty($collaboration)) {
            Flash::error('Collaboration not found');

            return redirect(route('admin.collaborations.index'));
        }

        return view('backend.collaborations.edit')->with('collaboration', $collaboration);
    }

    /**
     * Update the specified Collaboration in storage.
     *
     * @param  int              $id
     * @param UpdateCollaborationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCollaborationRequest $request)
    {
        $collaboration = $this->collaborationRepository->findWithoutFail($id);

        if (empty($collaboration)) {
            Flash::error('Collaboration not found');

            return redirect(route('admin.collaborations.index'));
        }

        $collaboration = $this->collaborationRepository->update($request->all(), $id);

        Flash::success('Collaboration updated successfully.');

        return redirect(route('admin.collaborations.index'));
    }

    /**
     * Remove the specified Collaboration from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $collaboration = $this->collaborationRepository->findWithoutFail($id);

        if (empty($collaboration)) {
            Flash::error('Collaboration not found');

            return redirect(route('admin.collaborations.index'));
        }

        $this->collaborationRepository->delete($id);

        Flash::success('Collaboration deleted successfully.');

        return redirect(route('admin.collaborations.index'));
    }
}
