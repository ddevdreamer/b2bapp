<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAttendeeRequest;
use App\Models\Role;
use App\Models\User;
use App\Notifications\SendAuthCredentials;
use App\Repositories\AttendeeRepository;
use Hash;

class RegistrationController extends Controller
{
	public function store(CreateAttendeeRequest $request)
	{
//		return $request->all();

		$user = new User();
		$user->name = $request->input('first_name') . ' ' . $request->input('surname');
		$user->email = $request->input('email');
		$plain_password = str_random(8);
		$user->password = Hash::make($plain_password);

		$user->save();

		$request->merge(['user_id' => $user->id]);
		$attendeeRepo = app()->make(AttendeeRepository::class);
		$attendee = $attendeeRepo->create($request->all());

		$attendee->collaborations()->sync($request->input('collaborationItems'));

		$attendeeRole = Role::where('name', 'attendee')->first();
		$user->attachRole($attendeeRole);



		$info = [];
		$info['username'] = $user->email;
		$info['password'] = $plain_password;
		$user->notify(new SendAuthCredentials($info));

		flash('Registration Successful, Check your inbox for login credentials.');

		return redirect()->back();
	}
}
