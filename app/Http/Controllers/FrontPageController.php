<?php

namespace App\Http\Controllers;

use App\Models\Collaboration;
use App\Models\Event;
use App\Repositories\BusinessSectorRepository;
use App\Repositories\EventRepository;
use App\Repositories\JobPositionRepository;
use Illuminate\Http\Request;

class FrontPageController extends Controller
{
	public function homepage()
	{
		$recent_events = $this->getRecentEvents();

//		\Debugbar::addMessage($recent_events->toJson());

//		return $recent_events;

		return view('frontend.home')
			->with('recent_events', $recent_events);
	}

	public function aboutUs()
	{
		return view('frontend.about');
	}

	public function events()
	{
		$eventsRepository = app()->make(EventRepository::class);
		$events = $eventsRepository->orderBy('starts_at', 'asc')->all();

//		\Debugbar::addMessage($events->toJson());

//		return $events;

		return view('frontend.events')
			->with('events', $events);
	}

	public function event_detail($id)
	{
		$eventsRepository = app()->make(EventRepository::class);
		/** @var Event $event */
		$event = $eventsRepository->with('registered_attendees')->find($id);

		$eventInterestInfo = [];

		if ($this->signedIn && $this->currentUser->hasRole('attendee') && $event->registered_attendees->contains($this->currentUser->attendee->id)) {
			$eventInterestInfo = $event->registered_attendees
				->find($this->currentUser->attendee->id)->pivot;
		}

//		return $eventInterestInfo;

		return view('frontend.event_detail')
			->with('eventInterestInfo', $eventInterestInfo)
			->with('event', $event);
	}

	public function registration()
	{

		if ((\Auth::user() && !\Auth::user()->hasRole('admin'))) {
			\Flash::error('You are already registered!');

			return redirect('/');
		}

		$collaborations = Collaboration::all()->pluck('name', 'id');

		$jobPositionsRepo = app()->make(JobPositionRepository::class);
		$jobPositions = $jobPositionsRepo->all()->pluck('title', 'id');

		$businessSectorsRepo = app()->make(BusinessSectorRepository::class);
		$businessSectors = $businessSectorsRepo->all()->pluck('name', 'id');

		return view('frontend.registration')
			->with('businessSectors', $businessSectors)
			->with('jobPositions', $jobPositions)
			->with('collaborations', $collaborations);
	}

	public function postRegistration(Request $request)
	{
		return $request->all();
	}

	public function getRecentEvents()
	{
		$eventsRepo = app()->make(EventRepository::class);
		$events = $eventsRepo->with(['venue'])
			->all();

		return $events;
	}
}
