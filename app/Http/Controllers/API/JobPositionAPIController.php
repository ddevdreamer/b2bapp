<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateJobPositionAPIRequest;
use App\Http\Requests\API\UpdateJobPositionAPIRequest;
use App\Models\JobPosition;
use App\Repositories\JobPositionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class JobPositionController
 * @package App\Http\Controllers\API
 */

class JobPositionAPIController extends AppBaseController
{
    /** @var  JobPositionRepository */
    private $jobPositionRepository;

    public function __construct(JobPositionRepository $jobPositionRepo)
    {
        $this->jobPositionRepository = $jobPositionRepo;
    }

    /**
     * Display a listing of the JobPosition.
     * GET|HEAD /jobPositions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->jobPositionRepository->pushCriteria(new RequestCriteria($request));
        $this->jobPositionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $jobPositions = $this->jobPositionRepository->all();

        return $this->sendResponse($jobPositions->toArray(), 'Job Positions retrieved successfully');
    }

    /**
     * Store a newly created JobPosition in storage.
     * POST /jobPositions
     *
     * @param CreateJobPositionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateJobPositionAPIRequest $request)
    {
        $input = $request->all();

        $jobPositions = $this->jobPositionRepository->create($input);

        return $this->sendResponse($jobPositions->toArray(), 'Job Position saved successfully');
    }

    /**
     * Display the specified JobPosition.
     * GET|HEAD /jobPositions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var JobPosition $jobPosition */
        $jobPosition = $this->jobPositionRepository->findWithoutFail($id);

        if (empty($jobPosition)) {
            return $this->sendError('Job Position not found');
        }

        return $this->sendResponse($jobPosition->toArray(), 'Job Position retrieved successfully');
    }

    /**
     * Update the specified JobPosition in storage.
     * PUT/PATCH /jobPositions/{id}
     *
     * @param  int $id
     * @param UpdateJobPositionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobPositionAPIRequest $request)
    {
        $input = $request->all();

        /** @var JobPosition $jobPosition */
        $jobPosition = $this->jobPositionRepository->findWithoutFail($id);

        if (empty($jobPosition)) {
            return $this->sendError('Job Position not found');
        }

        $jobPosition = $this->jobPositionRepository->update($input, $id);

        return $this->sendResponse($jobPosition->toArray(), 'JobPosition updated successfully');
    }

    /**
     * Remove the specified JobPosition from storage.
     * DELETE /jobPositions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var JobPosition $jobPosition */
        $jobPosition = $this->jobPositionRepository->findWithoutFail($id);

        if (empty($jobPosition)) {
            return $this->sendError('Job Position not found');
        }

        $jobPosition->delete();

        return $this->sendResponse($id, 'Job Position deleted successfully');
    }
}
