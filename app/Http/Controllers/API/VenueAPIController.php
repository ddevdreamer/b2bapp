<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVenueAPIRequest;
use App\Http\Requests\API\UpdateVenueAPIRequest;
use App\Models\Venue;
use App\Repositories\VenueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class VenueController
 * @package App\Http\Controllers\API
 */

class VenueAPIController extends AppBaseController
{
    /** @var  VenueRepository */
    private $venueRepository;

    public function __construct(VenueRepository $venueRepo)
    {
        $this->venueRepository = $venueRepo;
    }

    /**
     * Display a listing of the Venue.
     * GET|HEAD /venues
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->venueRepository->pushCriteria(new RequestCriteria($request));
        $this->venueRepository->pushCriteria(new LimitOffsetCriteria($request));
        $venues = $this->venueRepository->all();

        return $this->sendResponse($venues->toArray(), 'Venues retrieved successfully');
    }

    /**
     * Store a newly created Venue in storage.
     * POST /venues
     *
     * @param CreateVenueAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVenueAPIRequest $request)
    {
        $input = $request->all();

        $venues = $this->venueRepository->create($input);

        return $this->sendResponse($venues->toArray(), 'Venue saved successfully');
    }

    /**
     * Display the specified Venue.
     * GET|HEAD /venues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Venue $venue */
        $venue = $this->venueRepository->findWithoutFail($id);

        if (empty($venue)) {
            return $this->sendError('Venue not found');
        }

        return $this->sendResponse($venue->toArray(), 'Venue retrieved successfully');
    }

    /**
     * Update the specified Venue in storage.
     * PUT/PATCH /venues/{id}
     *
     * @param  int $id
     * @param UpdateVenueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVenueAPIRequest $request)
    {
        $input = $request->all();

        /** @var Venue $venue */
        $venue = $this->venueRepository->findWithoutFail($id);

        if (empty($venue)) {
            return $this->sendError('Venue not found');
        }

        $venue = $this->venueRepository->update($input, $id);

        return $this->sendResponse($venue->toArray(), 'Venue updated successfully');
    }

    /**
     * Remove the specified Venue from storage.
     * DELETE /venues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Venue $venue */
        $venue = $this->venueRepository->findWithoutFail($id);

        if (empty($venue)) {
            return $this->sendError('Venue not found');
        }

        $venue->delete();

        return $this->sendResponse($id, 'Venue deleted successfully');
    }
}
