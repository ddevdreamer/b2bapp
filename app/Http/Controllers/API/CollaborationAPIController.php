<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCollaborationAPIRequest;
use App\Http\Requests\API\UpdateCollaborationAPIRequest;
use App\Models\Collaboration;
use App\Repositories\CollaborationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CollaborationController
 * @package App\Http\Controllers\API
 */

class CollaborationAPIController extends AppBaseController
{
    /** @var  CollaborationRepository */
    private $collaborationRepository;

    public function __construct(CollaborationRepository $collaborationRepo)
    {
        $this->collaborationRepository = $collaborationRepo;
    }

    /**
     * Display a listing of the Collaboration.
     * GET|HEAD /collaborations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->collaborationRepository->pushCriteria(new RequestCriteria($request));
        $this->collaborationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $collaborations = $this->collaborationRepository->all();

        return $this->sendResponse($collaborations->toArray(), 'Collaborations retrieved successfully');
    }

    /**
     * Store a newly created Collaboration in storage.
     * POST /collaborations
     *
     * @param CreateCollaborationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCollaborationAPIRequest $request)
    {
        $input = $request->all();

        $collaborations = $this->collaborationRepository->create($input);

        return $this->sendResponse($collaborations->toArray(), 'Collaboration saved successfully');
    }

    /**
     * Display the specified Collaboration.
     * GET|HEAD /collaborations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Collaboration $collaboration */
        $collaboration = $this->collaborationRepository->findWithoutFail($id);

        if (empty($collaboration)) {
            return $this->sendError('Collaboration not found');
        }

        return $this->sendResponse($collaboration->toArray(), 'Collaboration retrieved successfully');
    }

    /**
     * Update the specified Collaboration in storage.
     * PUT/PATCH /collaborations/{id}
     *
     * @param  int $id
     * @param UpdateCollaborationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCollaborationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Collaboration $collaboration */
        $collaboration = $this->collaborationRepository->findWithoutFail($id);

        if (empty($collaboration)) {
            return $this->sendError('Collaboration not found');
        }

        $collaboration = $this->collaborationRepository->update($input, $id);

        return $this->sendResponse($collaboration->toArray(), 'Collaboration updated successfully');
    }

    /**
     * Remove the specified Collaboration from storage.
     * DELETE /collaborations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Collaboration $collaboration */
        $collaboration = $this->collaborationRepository->findWithoutFail($id);

        if (empty($collaboration)) {
            return $this->sendError('Collaboration not found');
        }

        $collaboration->delete();

        return $this->sendResponse($id, 'Collaboration deleted successfully');
    }
}
