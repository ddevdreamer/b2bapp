<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMeetingStatusAPIRequest;
use App\Http\Requests\API\UpdateMeetingStatusAPIRequest;
use App\Models\MeetingStatus;
use App\Repositories\MeetingStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MeetingStatusController
 * @package App\Http\Controllers\API
 */

class MeetingStatusAPIController extends AppBaseController
{
    /** @var  MeetingStatusRepository */
    private $meetingStatusRepository;

    public function __construct(MeetingStatusRepository $meetingStatusRepo)
    {
        $this->meetingStatusRepository = $meetingStatusRepo;
    }

    /**
     * Display a listing of the MeetingStatus.
     * GET|HEAD /meetingStatuses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->meetingStatusRepository->pushCriteria(new RequestCriteria($request));
        $this->meetingStatusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $meetingStatuses = $this->meetingStatusRepository->all();

        return $this->sendResponse($meetingStatuses->toArray(), 'Meeting Statuses retrieved successfully');
    }

    /**
     * Store a newly created MeetingStatus in storage.
     * POST /meetingStatuses
     *
     * @param CreateMeetingStatusAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMeetingStatusAPIRequest $request)
    {
        $input = $request->all();

        $meetingStatuses = $this->meetingStatusRepository->create($input);

        return $this->sendResponse($meetingStatuses->toArray(), 'Meeting Status saved successfully');
    }

    /**
     * Display the specified MeetingStatus.
     * GET|HEAD /meetingStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MeetingStatus $meetingStatus */
        $meetingStatus = $this->meetingStatusRepository->findWithoutFail($id);

        if (empty($meetingStatus)) {
            return $this->sendError('Meeting Status not found');
        }

        return $this->sendResponse($meetingStatus->toArray(), 'Meeting Status retrieved successfully');
    }

    /**
     * Update the specified MeetingStatus in storage.
     * PUT/PATCH /meetingStatuses/{id}
     *
     * @param  int $id
     * @param UpdateMeetingStatusAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMeetingStatusAPIRequest $request)
    {
        $input = $request->all();

        /** @var MeetingStatus $meetingStatus */
        $meetingStatus = $this->meetingStatusRepository->findWithoutFail($id);

        if (empty($meetingStatus)) {
            return $this->sendError('Meeting Status not found');
        }

        $meetingStatus = $this->meetingStatusRepository->update($input, $id);

        return $this->sendResponse($meetingStatus->toArray(), 'MeetingStatus updated successfully');
    }

    /**
     * Remove the specified MeetingStatus from storage.
     * DELETE /meetingStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MeetingStatus $meetingStatus */
        $meetingStatus = $this->meetingStatusRepository->findWithoutFail($id);

        if (empty($meetingStatus)) {
            return $this->sendError('Meeting Status not found');
        }

        $meetingStatus->delete();

        return $this->sendResponse($id, 'Meeting Status deleted successfully');
    }
}
