<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBusinessSectorAPIRequest;
use App\Http\Requests\API\UpdateBusinessSectorAPIRequest;
use App\Models\BusinessSector;
use App\Repositories\BusinessSectorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BusinessSectorController
 * @package App\Http\Controllers\API
 */

class BusinessSectorAPIController extends AppBaseController
{
    /** @var  BusinessSectorRepository */
    private $businessSectorRepository;

    public function __construct(BusinessSectorRepository $businessSectorRepo)
    {
        $this->businessSectorRepository = $businessSectorRepo;
    }

    /**
     * Display a listing of the BusinessSector.
     * GET|HEAD /businessSectors
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->businessSectorRepository->pushCriteria(new RequestCriteria($request));
        $this->businessSectorRepository->pushCriteria(new LimitOffsetCriteria($request));
        $businessSectors = $this->businessSectorRepository->all();

        return $this->sendResponse($businessSectors->toArray(), 'Business Sectors retrieved successfully');
    }

    /**
     * Store a newly created BusinessSector in storage.
     * POST /businessSectors
     *
     * @param CreateBusinessSectorAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBusinessSectorAPIRequest $request)
    {
        $input = $request->all();

        $businessSectors = $this->businessSectorRepository->create($input);

        return $this->sendResponse($businessSectors->toArray(), 'Business Sector saved successfully');
    }

    /**
     * Display the specified BusinessSector.
     * GET|HEAD /businessSectors/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BusinessSector $businessSector */
        $businessSector = $this->businessSectorRepository->findWithoutFail($id);

        if (empty($businessSector)) {
            return $this->sendError('Business Sector not found');
        }

        return $this->sendResponse($businessSector->toArray(), 'Business Sector retrieved successfully');
    }

    /**
     * Update the specified BusinessSector in storage.
     * PUT/PATCH /businessSectors/{id}
     *
     * @param  int $id
     * @param UpdateBusinessSectorAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBusinessSectorAPIRequest $request)
    {
        $input = $request->all();

        /** @var BusinessSector $businessSector */
        $businessSector = $this->businessSectorRepository->findWithoutFail($id);

        if (empty($businessSector)) {
            return $this->sendError('Business Sector not found');
        }

        $businessSector = $this->businessSectorRepository->update($input, $id);

        return $this->sendResponse($businessSector->toArray(), 'BusinessSector updated successfully');
    }

    /**
     * Remove the specified BusinessSector from storage.
     * DELETE /businessSectors/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BusinessSector $businessSector */
        $businessSector = $this->businessSectorRepository->findWithoutFail($id);

        if (empty($businessSector)) {
            return $this->sendError('Business Sector not found');
        }

        $businessSector->delete();

        return $this->sendResponse($id, 'Business Sector deleted successfully');
    }
}
