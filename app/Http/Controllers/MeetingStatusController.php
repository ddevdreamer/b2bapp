<?php

namespace App\Http\Controllers;

use App\DataTables\MeetingStatusDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMeetingStatusRequest;
use App\Http\Requests\UpdateMeetingStatusRequest;
use App\Repositories\MeetingStatusRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MeetingStatusController extends AppBaseController
{
    /** @var  MeetingStatusRepository */
    private $meetingStatusRepository;

    public function __construct(MeetingStatusRepository $meetingStatusRepo)
    {
        $this->meetingStatusRepository = $meetingStatusRepo;
    }

    /**
     * Display a listing of the MeetingStatus.
     *
     * @param MeetingStatusDataTable $meetingStatusDataTable
     * @return Response
     */
    public function index(MeetingStatusDataTable $meetingStatusDataTable)
    {
        return $meetingStatusDataTable->render('backend.meeting_statuses.index');
    }

    /**
     * Show the form for creating a new MeetingStatus.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.meeting_statuses.create');
    }

    /**
     * Store a newly created MeetingStatus in storage.
     *
     * @param CreateMeetingStatusRequest $request
     *
     * @return Response
     */
    public function store(CreateMeetingStatusRequest $request)
    {
        $input = $request->all();

        $meetingStatus = $this->meetingStatusRepository->create($input);

        Flash::success('Meeting Status saved successfully.');

        return redirect(route('admin.meetingStatuses.index'));
    }

    /**
     * Display the specified MeetingStatus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $meetingStatus = $this->meetingStatusRepository->findWithoutFail($id);

        if (empty($meetingStatus)) {
            Flash::error('Meeting Status not found');

            return redirect(route('admin.meetingStatuses.index'));
        }

        return view('backend.meeting_statuses.show')->with('meetingStatus', $meetingStatus);
    }

    /**
     * Show the form for editing the specified MeetingStatus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $meetingStatus = $this->meetingStatusRepository->findWithoutFail($id);

        if (empty($meetingStatus)) {
            Flash::error('Meeting Status not found');

            return redirect(route('admin.meetingStatuses.index'));
        }

        return view('backend.meeting_statuses.edit')->with('meetingStatus', $meetingStatus);
    }

    /**
     * Update the specified MeetingStatus in storage.
     *
     * @param  int              $id
     * @param UpdateMeetingStatusRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMeetingStatusRequest $request)
    {
        $meetingStatus = $this->meetingStatusRepository->findWithoutFail($id);

        if (empty($meetingStatus)) {
            Flash::error('Meeting Status not found');

            return redirect(route('admin.meetingStatuses.index'));
        }

        $meetingStatus = $this->meetingStatusRepository->update($request->all(), $id);

        Flash::success('Meeting Status updated successfully.');

        return redirect(route('admin.meetingStatuses.index'));
    }

    /**
     * Remove the specified MeetingStatus from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $meetingStatus = $this->meetingStatusRepository->findWithoutFail($id);

        if (empty($meetingStatus)) {
            Flash::error('Meeting Status not found');

            return redirect(route('admin.meetingStatuses.index'));
        }

        $this->meetingStatusRepository->delete($id);

        Flash::success('Meeting Status deleted successfully.');

        return redirect(route('admin.meetingStatuses.index'));
    }
}
