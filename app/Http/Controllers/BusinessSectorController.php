<?php

namespace App\Http\Controllers;

use App\DataTables\BusinessSectorDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBusinessSectorRequest;
use App\Http\Requests\UpdateBusinessSectorRequest;
use App\Repositories\BusinessSectorRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class BusinessSectorController extends AppBaseController
{
    /** @var  BusinessSectorRepository */
    private $businessSectorRepository;

    public function __construct(BusinessSectorRepository $businessSectorRepo)
    {
        $this->businessSectorRepository = $businessSectorRepo;
    }

    /**
     * Display a listing of the BusinessSector.
     *
     * @param BusinessSectorDataTable $businessSectorDataTable
     * @return Response
     */
    public function index(BusinessSectorDataTable $businessSectorDataTable)
    {
        return $businessSectorDataTable->render('backend.business_sectors.index');
    }

    /**
     * Show the form for creating a new BusinessSector.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.business_sectors.create');
    }

    /**
     * Store a newly created BusinessSector in storage.
     *
     * @param CreateBusinessSectorRequest $request
     *
     * @return Response
     */
    public function store(CreateBusinessSectorRequest $request)
    {
        $input = $request->all();

        $businessSector = $this->businessSectorRepository->create($input);

        Flash::success('Business Sector saved successfully.');

        return redirect(route('admin.businessSectors.index'));
    }

    /**
     * Display the specified BusinessSector.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $businessSector = $this->businessSectorRepository->findWithoutFail($id);

        if (empty($businessSector)) {
            Flash::error('Business Sector not found');

            return redirect(route('admin.businessSectors.index'));
        }

        return view('backend.business_sectors.show')->with('businessSector', $businessSector);
    }

    /**
     * Show the form for editing the specified BusinessSector.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $businessSector = $this->businessSectorRepository->findWithoutFail($id);

        if (empty($businessSector)) {
            Flash::error('Business Sector not found');

            return redirect(route('admin.businessSectors.index'));
        }

        return view('backend.business_sectors.edit')->with('businessSector', $businessSector);
    }

    /**
     * Update the specified BusinessSector in storage.
     *
     * @param  int              $id
     * @param UpdateBusinessSectorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBusinessSectorRequest $request)
    {
        $businessSector = $this->businessSectorRepository->findWithoutFail($id);

        if (empty($businessSector)) {
            Flash::error('Business Sector not found');

            return redirect(route('admin.businessSectors.index'));
        }

        $businessSector = $this->businessSectorRepository->update($request->all(), $id);

        Flash::success('Business Sector updated successfully.');

        return redirect(route('admin.businessSectors.index'));
    }

    /**
     * Remove the specified BusinessSector from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $businessSector = $this->businessSectorRepository->findWithoutFail($id);

        if (empty($businessSector)) {
            Flash::error('Business Sector not found');

            return redirect(route('admin.businessSectors.index'));
        }

        $this->businessSectorRepository->delete($id);

        Flash::success('Business Sector deleted successfully.');

        return redirect(route('admin.businessSectors.index'));
    }
}
