<?php

namespace App\Http\Controllers;

use App\DataTables\PictureDataTable;
use App\Http\Requests\CreatePictureRequest;
use App\Http\Requests\UpdatePictureRequest;
use App\Models\Event;
use App\Repositories\EventRepository;
use App\Repositories\PictureRepository;
use Flash;
use Response;

class PictureController extends AppBaseController
{
	/** @var  PictureRepository */
	private $pictureRepository;
	/**
	 * @var EventRepository
	 */
	private $eventRepo;

	public function __construct(PictureRepository $pictureRepo, EventRepository $eventRepo)
	{
		$this->pictureRepository = $pictureRepo;
		$this->eventRepo = $eventRepo;
	}

	/**
	 * Display a listing of the Picture.
	 *
	 * @param Event $event
	 * @param PictureDataTable $pictureDataTable
	 * @return Response
	 */
	public function index(Event $event, PictureDataTable $pictureDataTable)
	{

		return $pictureDataTable->forEvent($event)->render('backend.pictures.index', compact('event'));
	}

	/**
	 * Show the form for creating a new Picture.
	 *
	 * @param Event $event
	 * @return Response
	 */
	public function create(Event $event)
	{
		return view('backend.pictures.create')->with(compact('event'));
	}

	/**
	 * Store a newly created Picture in storage.
	 *
	 * @param Event $event
	 * @param CreatePictureRequest $request
	 * @return Response
	 */
	public function store(Event $event, CreatePictureRequest $request)
	{
		$request->merge(['event_id' => $event->id]);
		$input = $request->all();

		$picture = $this->pictureRepository->create($input);

		Flash::success('Picture saved successfully.');

		return redirect(route('admin.pictures.index'));
	}

	/**
	 * Display the specified Picture.
	 *
	 * @param Event $event
	 * @param  int $id
	 * @return Response
	 */
	public function show(Event $event, $id)
	{
		$picture = $this->pictureRepository->findWithoutFail($id);

		if (empty($picture)) {
			Flash::error('Picture not found');

			return redirect(route('admin.pictures.index'));
		}

		return view('backend.pictures.show')->with('picture', $picture)->with(compact('event'));
	}

	/**
	 * Show the form for editing the specified Picture.
	 *
	 * @param Event $event
	 * @param  int $id
	 * @return Response
	 */
	public function edit(Event $event, $id)
	{
		$picture = $this->pictureRepository->findWithoutFail($id);

		if (empty($picture)) {
			Flash::error('Picture not found');

			return redirect(route('admin.pictures.index'));
		}

		return view('backend.pictures.edit')->with('picture', $picture)->with(compact('event'));
	}

	/**
	 * Update the specified Picture in storage.
	 *
	 * @param Event $event
	 * @param  int $id
	 * @param UpdatePictureRequest $request
	 * @return Response
	 */
	public function update(Event $event, $id, UpdatePictureRequest $request)
	{
		$picture = $this->pictureRepository->findWithoutFail($id);

		if (empty($picture)) {
			Flash::error('Picture not found');

			return redirect(route('admin.pictures.index'));
		}

		$picture = $this->pictureRepository->update($request->all(), $id);

		Flash::success('Picture updated successfully.');

		return redirect(route('admin.pictures.index'));
	}

	/**
	 * Remove the specified Picture from storage.
	 *
	 * @param Event $event
	 * @param  int $id
	 * @return Response
	 */
	public function destroy(Event $event, $id)
	{
		$picture = $this->pictureRepository->findWithoutFail($id);

		if (empty($picture)) {
			Flash::error('Picture not found');

			return redirect(route('admin.pictures.index'));
		}

		$this->pictureRepository->delete($id);

		Flash::success('Picture deleted successfully.');

		return redirect(route('admin.pictures.index'));
	}
}
