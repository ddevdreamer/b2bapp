<?php

namespace App\Http\Controllers;

use App\Models\Attendee;
use App\Repositories\EventRepository;
use App\Repositories\MeetingRepository;
use DB;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
//		alert()->success('You are logged in.', 'Welcome!');

		if ($this->signedIn && $this->currentUser->hasRole('attendee')) {
			return view('home');
		}
		if ($this->signedIn && $this->currentUser->hasRole('admin')) {
			$registrations_by_country = Attendee::with([
				'country' => function ($q) {
					$q->select('id', 'name', 'flag', 'iso_3166_2');
				}
			])
				->select('country_id', DB::raw('count(*) as total'))
				->groupBy('country_id')
				->orderBy('total', 'desc')
				->get();

			$totalRegistrations = $registrations_by_country->sum('total');
//		return $totalRegistrations;
			$registrations_by_country->each(function ($registration) use ($totalRegistrations) {
				$registration->percentage = round(($registration->total / $totalRegistrations) * 100, 2);
			});

			$mapData = $registrations_by_country->pluck('total', 'country.iso_3166_2');

//		return $registrations_by_country;

			$eventsRepo = app()->make(EventRepository::class);
			$events = $eventsRepo->all();

			$meetingsRepo = app()->make(MeetingRepository::class);
			$meetings = $meetingsRepo->all();

			return view('home')
				->with('registrations_by_country', $registrations_by_country)
				->with(compact('mapData'))
				->with(compact('events'))
				->with(compact('meetings'));
		}

	}
}
