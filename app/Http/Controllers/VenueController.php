<?php

namespace App\Http\Controllers;

use App\DataTables\VenueDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateVenueRequest;
use App\Http\Requests\UpdateVenueRequest;
use App\Repositories\VenueRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class VenueController extends AppBaseController
{
    /** @var  VenueRepository */
    private $venueRepository;

    public function __construct(VenueRepository $venueRepo)
    {
        $this->venueRepository = $venueRepo;
    }

    /**
     * Display a listing of the Venue.
     *
     * @param VenueDataTable $venueDataTable
     * @return Response
     */
    public function index(VenueDataTable $venueDataTable)
    {
        return $venueDataTable->render('backend.venues.index');
    }

    /**
     * Show the form for creating a new Venue.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.venues.create');
    }

    /**
     * Store a newly created Venue in storage.
     *
     * @param CreateVenueRequest $request
     *
     * @return Response
     */
    public function store(CreateVenueRequest $request)
    {
        $input = $request->all();

        $venue = $this->venueRepository->create($input);

        Flash::success('Venue saved successfully.');

        return redirect(route('admin.venues.index'));
    }

    /**
     * Display the specified Venue.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $venue = $this->venueRepository->findWithoutFail($id);

        if (empty($venue)) {
            Flash::error('Venue not found');

            return redirect(route('admin.venues.index'));
        }

        return view('backend.venues.show')->with('venue', $venue);
    }

    /**
     * Show the form for editing the specified Venue.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $venue = $this->venueRepository->findWithoutFail($id);

        if (empty($venue)) {
            Flash::error('Venue not found');

            return redirect(route('admin.venues.index'));
        }

        return view('backend.venues.edit')->with('venue', $venue);
    }

    /**
     * Update the specified Venue in storage.
     *
     * @param  int              $id
     * @param UpdateVenueRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVenueRequest $request)
    {
        $venue = $this->venueRepository->findWithoutFail($id);

        if (empty($venue)) {
            Flash::error('Venue not found');

            return redirect(route('admin.venues.index'));
        }

        $venue = $this->venueRepository->update($request->all(), $id);

        Flash::success('Venue updated successfully.');

        return redirect(route('admin.venues.index'));
    }

    /**
     * Remove the specified Venue from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $venue = $this->venueRepository->findWithoutFail($id);

        if (empty($venue)) {
            Flash::error('Venue not found');

            return redirect(route('admin.venues.index'));
        }

        $this->venueRepository->delete($id);

        Flash::success('Venue deleted successfully.');

        return redirect(route('admin.venues.index'));
    }
}
