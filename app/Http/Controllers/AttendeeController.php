<?php

namespace App\Http\Controllers;

use App\DataTables\AttendeeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAttendeeRequest;
use App\Http\Requests\UpdateAttendeeRequest;
use App\Repositories\AttendeeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AttendeeController extends AppBaseController
{
    /** @var  AttendeeRepository */
    private $attendeeRepository;

    public function __construct(AttendeeRepository $attendeeRepo)
    {
        $this->attendeeRepository = $attendeeRepo;
    }

    /**
     * Display a listing of the Attendee.
     *
     * @param AttendeeDataTable $attendeeDataTable
     * @return Response
     */
    public function index(AttendeeDataTable $attendeeDataTable)
    {
        return $attendeeDataTable->render('backend.attendees.index');
    }

    /**
     * Show the form for creating a new Attendee.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.attendees.create');
    }

    /**
     * Store a newly created Attendee in storage.
     *
     * @param CreateAttendeeRequest $request
     *
     * @return Response
     */
    public function store(CreateAttendeeRequest $request)
    {
        $input = $request->all();

        $attendee = $this->attendeeRepository->create($input);

        Flash::success('Attendee saved successfully.');

        return redirect(route('admin.attendees.index'));
    }

    /**
     * Display the specified Attendee.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $attendee = $this->attendeeRepository->findWithoutFail($id);

        if (empty($attendee)) {
            Flash::error('Attendee not found');

            return redirect(route('admin.attendees.index'));
        }

        return view('backend.attendees.show')->with('attendee', $attendee);
    }

    /**
     * Show the form for editing the specified Attendee.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $attendee = $this->attendeeRepository->findWithoutFail($id);

        if (empty($attendee)) {
            Flash::error('Attendee not found');

            return redirect(route('admin.attendees.index'));
        }

        return view('backend.attendees.edit')->with('attendee', $attendee);
    }

    /**
     * Update the specified Attendee in storage.
     *
     * @param  int              $id
     * @param UpdateAttendeeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAttendeeRequest $request)
    {
        $attendee = $this->attendeeRepository->findWithoutFail($id);

        if (empty($attendee)) {
            Flash::error('Attendee not found');

            return redirect(route('admin.attendees.index'));
        }

        $attendee = $this->attendeeRepository->update($request->all(), $id);

        Flash::success('Attendee updated successfully.');

        return redirect(route('admin.attendees.index'));
    }

    /**
     * Remove the specified Attendee from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $attendee = $this->attendeeRepository->findWithoutFail($id);

        if (empty($attendee)) {
            Flash::error('Attendee not found');

            return redirect(route('admin.attendees.index'));
        }

        $this->attendeeRepository->delete($id);

        Flash::success('Attendee deleted successfully.');

        return redirect(route('admin.attendees.index'));
    }
}
