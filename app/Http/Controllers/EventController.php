<?php

namespace App\Http\Controllers;

use App\DataTables\EventDataTable;
use App\DataTables\EventRegisteredAttendeeDataTable;
use App\Http\Requests\ArrangeMeetingRequest;
use App\Http\Requests\CreateEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Models\Attendee;
use App\Models\Event;
use App\Models\Meeting;
use App\Repositories\AttendeeRepository;
use App\Repositories\EventRepository;
use App\Repositories\MeetingRepository;
use App\Repositories\VenueRepository;
use Flash;
use Response;

class EventController extends AppBaseController
{
	/** @var  EventRepository */
	private $eventRepository;

	public function __construct(EventRepository $eventRepo)
	{
		$this->eventRepository = $eventRepo;
	}

	/**
	 * Display a listing of the Event.
	 *
	 * @param EventDataTable $eventDataTable
	 * @return Response
	 */
	public function index(EventDataTable $eventDataTable)
	{
		return $eventDataTable->render('backend.events.index');
	}

	/**
	 * Show the form for creating a new Event.
	 *
	 * @return Response
	 */
	public function create()
	{
		/** @var VenueRepository $venueRepo */
		$venueRepo = app()->make(VenueRepository::class);
		$venues = $venueRepo->all()->pluck('name', 'id');

		return view('backend.events.create')
			->with(compact('venues'));
	}

	/**
	 * Store a newly created Event in storage.
	 *
	 * @param CreateEventRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateEventRequest $request)
	{
		$input = $request->all();

		$event = $this->eventRepository->create($input);

		Flash::success('Event saved successfully.');

		return redirect(route('admin.events.index'));
	}

	/**
	 * Display the specified Event.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$event = $this->eventRepository->findWithoutFail($id);

		if (empty($event)) {
			Flash::error('Event not found');

			return redirect(route('admin.events.index'));
		}

		return view('backend.events.show')->with('event', $event);
	}

	/**
	 * Show the form for editing the specified Event.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$event = $this->eventRepository->findWithoutFail($id);

		if (empty($event)) {
			Flash::error('Event not found');

			return redirect(route('admin.events.index'));
		}

		/** @var VenueRepository $venueRepo */
		$venueRepo = app()->make(VenueRepository::class);
		$venues = $venueRepo->all()->pluck('name', 'id');

		return view('backend.events.edit')
			->with('event', $event)
			->with(compact('venues'));

	}

	/**
	 * Update the specified Event in storage.
	 *
	 * @param  int $id
	 * @param UpdateEventRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateEventRequest $request)
	{
		$event = $this->eventRepository->findWithoutFail($id);

		if (empty($event)) {
			Flash::error('Event not found');

			return redirect(route('admin.events.index'));
		}

		$event = $this->eventRepository->update($request->all(), $id);

		Flash::success('Event updated successfully.');

		return redirect(route('admin.events.index'));
	}

	/**
	 * Remove the specified Event from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$event = $this->eventRepository->findWithoutFail($id);

		if (empty($event)) {
			Flash::error('Event not found');

			return redirect(route('admin.events.index'));
		}

		$this->eventRepository->delete($id);

		Flash::success('Event deleted successfully.');

		return redirect(route('admin.events.index'));
	}

	public function registeredUsers(Event $event, EventRegisteredAttendeeDataTable $dataTable)
	{

//		$attendees = Attendee::whereHas('registered_events', function ($q) use ($event) {
//			$q->where('event_id', $event->id);
//		})->get();

//		return $attendees;

//		$event->load('registered_attendees', 'registered_attendees.job_position', 'registered_attendees.country');

		return $dataTable->forEvent($event)->render('backend.event_registered_attendees.index', compact('event'));
	}

	public function registeredUsersOld($id)
	{
		$eventRepo = app()->make(EventRepository::class);

		$event = $eventRepo->with('registered_attendee')->findWithoutFail($id);
		$event->registered_users->load('attendee', 'attendee.job_position', 'attendee.country', 'attendee.business_sector');
		$registered_users = $event->registered_users;

		return view('backend.events.registered_users')
			->with('event', $event)
			->with('registered_users', $registered_users);
	}

	public function arrange_meeting($event_id, $attendee_id)
	{
//		$user = User::with(['attendee', 'attendee', 'attendee.job_position', 'attendee.country', 'attendee.business_sector'])->find($user_id);

		$attendee = Attendee::with(['job_position', 'country', 'business_sector'])->find($attendee_id);

		$eventsRepo = app()->make(EventRepository::class);
		$event = $eventsRepo->findWithoutFail($event_id);

		$event->registered_attendees->load('job_position', 'country', 'business_sector');

		$registered_attendees = $event->registered_attendees->keyBy('id')->forget($attendee->id)->pluck('first_name', 'id');

//		return $registered_attendees;

		return view('backend.events.arrange_meeting')
			->with('first_attendee', $attendee)
			->with('second_attendees', $registered_attendees)
			->with('event', $event);
	}

	public function arrange_meetingPost(ArrangeMeetingRequest $request, $event_id, $first_attendee_id)
	{
//		dd($request->all());

		$eventsRepo = app()->make(EventRepository::class);
		$event = $eventsRepo->findWithoutFail($event_id);

		if (empty($event)) {
			Flash::error('Event not found');

			return redirect()->back();
		}

		/** @var AttendeeRepository $attendeeRepo */
		$attendeeRepo = app()->make(AttendeeRepository::class);
		$first_attendee = $attendeeRepo->findWithoutFail($first_attendee_id);
		$second_attendee = $attendeeRepo->findWithoutFail(request('second_attendee'));

		if ($first_attendee && $second_attendee && $event) {
			/** @var MeetingRepository $meetingRepo */
			$meetingRepo = app()->make(MeetingRepository::class);

//			$meeting_allowed = $meetingRepo->checkForMeetingConflict($event, $first_attendee, $second_attendee, $request);
//			if ($meeting_allowed) {
//			$meeting = $meetingRepo->create([
//				'event_id'           => $event->id,
//				'first_attendee_id'  => $first_attendee->id,
//				'second_attendee_id' => $second_attendee->id,
//				'starts_at'          => $request->input('meeting_starts_at'),
//				'ends_at'            => $request->input('meeting_ends_at'),
//				'meeting_status_id'  => 1
//			]);
//			}

			$meeting = new Meeting([
				'event_id'           => $event->id,
				'first_attendee_id'  => $first_attendee->id,
				'second_attendee_id' => $second_attendee->id,
				'starts_at'          => $request->input('meeting_starts_at'),
				'ends_at'            => $request->input('meeting_ends_at'),
				'meeting_status_id'  => 1
			]);

			$meeting->save();

			return redirect()->route('admin.meetings.index');
		} else {
			Flash::error('Error');

			return redirect()->back();
		}


	}
}
