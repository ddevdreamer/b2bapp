<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @property User|null currentUser
 * @property bool signedIn
 */
class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function __construct()
	{
		$this->middleware(/**
		 * @param $request
		 * @param $next
		 * @return mixed
		 */
			function ($request, $next) {
			$this->currentUser = auth()->user();
			$this->signedIn = auth()->check();

			view()->share('signedIn', $this->signedIn);
			view()->share('currentUser', $this->currentUser);

			return $next($request);
		});
	}

}
