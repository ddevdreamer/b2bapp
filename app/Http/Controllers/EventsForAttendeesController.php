<?php

namespace App\Http\Controllers;

use Alert;
use App\Notifications\RegisteredForEvent;
use App\Repositories\EventRepository;
use Flash;
use Illuminate\Http\Request;

class EventsForAttendeesController extends Controller
{

	public function events_list()
	{
		$eventsRepo = app()->make(EventRepository::class);
		$events = $eventsRepo->all();

		$registeredEvents = $this->currentUser->attendee->registered_events;

//		return $registeredEvents;

		return view('backend.users.events_list')
			->with('registeredEvents', $registeredEvents)
			->with('events', $events);
	}

	public function registeredEvents()
	{
		$events = $this->currentUser->attendee->registered_events;

		return view('backend.users.registered_events')
			->with('events', $events);
	}

	public function registerForEvent($event_id)
	{
		$eventsRepo = app()->make(EventRepository::class);

		$event = $eventsRepo->findWithoutFail($event_id);

		if (empty($event)) {
			Alert::error('Event Not Found');

			return redirect()->back();
		}
		$this->currentUser->load(['attendee', 'attendee.registered_events']);

//		return $this->currentUser;
		$registered_events = $this->currentUser->attendee->registered_events;
		if ($registered_events && $registered_events->contains($event_id)) {
			Flash::error('Already registered for this event!');

			return redirect()->back();
		}


		$this->currentUser->attendee->registered_events()->attach($event->id);

//		$this->currentUser->attendee();

		$this->currentUser->notify(new RegisteredForEvent($event));

		Alert::success('Registered for Event Successfully!');

		return redirect()->route('frontend.event_detail', [$event->id]);

	}

	public function registerEventInterest(Request $request, $event_id)
	{
//		return $request->all();

		$eventsRepo = app()->make(EventRepository::class);

		/** @var Event $event */
		$event = $eventsRepo->findWithoutFail($event_id);

		if (empty($event)) {
			Flash::error('Event Not Found');

			return redirect()->back();
		}
		if (!$this->currentUser->attendee->registered_events->contains($event_id)) {
			Flash::error('You haven\'t registered for this event!');

			return redirect()->back();
		}

//		$event->registered_users()->updateExistingPivot(\Auth::user()->id, [
//			'company'    => $request->input('company'),
//			'name'       => $request->input('name'),
//			'working_in' => $request->input('working_in')
//		]);

		return redirect()->back();
	}
}
