<?php

namespace App\Http\Requests;

use App\Models\Event;
use Illuminate\Foundation\Http\FormRequest;

class ArrangeMeetingRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$event = Event::find($this->route('event'));

//		dd($event);
//		dd($this->route('event'));
		$starts_at_time = $event->starts_at->copy()->second(0)->subSecond();
		$ends_at_time = $event->ends_at->copy()->second(0)->addSecond();

		return [
			"second_attendee"   => "required|exists:attendees,id",
			"meeting_starts_at" => "required|after:" . $starts_at_time . "|before:" . $ends_at_time,
			"meeting_ends_at"   => "required|after:meeting_starts_at|before:" . $ends_at_time
		];
	}
}
