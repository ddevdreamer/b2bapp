<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * @property  username
 */
class SendAuthCredentials extends Notification
{
	use Queueable;
	/**
	 * @var
	 */
	public $username, $password;

	/**
	 * Create a new notification instance.
	 *
	 * @param $info
	 */
	public function __construct($info)
	{
		$this->username = $info['username'];
		$this->password = $info['password'];
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		return (new MailMessage)
			->subject('Login Credentials')
			->line('Welcome to Scheduling Platform.')
			->line('Username: ' . $this->username)
			->line('Password: ' . $this->password)
			->action('Access your account', env('APP_URL') . '/login')
			->line('Thank you.');
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
