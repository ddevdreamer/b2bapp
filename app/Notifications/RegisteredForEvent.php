<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RegisteredForEvent extends Notification
{
	use Queueable;
	/**
	 * @var
	 */
	private $event;

	/**
	 * Create a new notification instance.
	 *
	 * @param $event
	 */
	public function __construct($event)
	{
		//
		$this->event = $event;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		return (new MailMessage)
			->success('Event Registration Notification')
			->line('Thank you for Registering for the event - ' . $this->event->title)
			->action('Event Information', route('frontend.event_detail', [$this->event->id]))
			->line('Thank you for using Scheduling Platform!');
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
