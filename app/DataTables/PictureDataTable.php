<?php

namespace App\DataTables;

use App\Models\Picture;
use Yajra\Datatables\Services\DataTable;

class PictureDataTable extends DataTable
{
	protected $event;

	public function forEvent($event)
	{
		$this->event = $event;

		return $this;
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax()
	{
		return $this->datatables
			->eloquent($this->query())
			->addColumn('action', function ($row) {
				return view('backend.pictures.datatables_actions', $row)
					->with('event', $this->event)
					->render();

			})
			->make(true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query()
	{
		$pictures = Picture::query();

		return $this->applyScopes($pictures);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->columns($this->getColumns())
			->addAction(['width' => '10%'])
			->ajax('')
			->parameters([
				'dom'     => 'Bfrtip',
				'scrollX' => false,
				'buttons' => [
					'print',
					'reset',
					'reload',
					[
						'extend'  => 'collection',
						'text'    => '<i class="fa fa-download"></i> Export',
						'buttons' => [
							'csv',
							'excel',
							'pdf',
						],
					],
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns()
	{
		return [
			'name'       => ['name' => 'name', 'data' => 'name'],
			'image_path' => ['name' => 'image_path', 'data' => 'image_path'],
			'featured'   => ['name' => 'featured', 'data' => 'featured']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'pictures';
	}
}
