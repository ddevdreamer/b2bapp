<?php

namespace App\DataTables;

use App\Models\Attendee;
use App\Models\Event;
use Yajra\Datatables\Services\DataTable;

class EventRegisteredAttendeeDataTable extends DataTable
{

	protected $event;

	/**
	 * @param Event $event
	 * @return $this
	 */
	public function forEvent(Event $event)
	{
		$this->event = $event;

		return $this;
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax()
	{
		$event = $this->event;

		return $this->datatables
			->eloquent($this->query())
			->addColumn('action', function ($row) use ($event) {
				return view('backend.event_registered_attendees.datatables_actions', $row)
					->with('event', $event)
					->render();
			})
			->make(true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query()
	{

		$event = $this->event;
		$attendees = Attendee::with('job_position', 'country')
			->whereHas('registered_events', function ($q) use ($event) {
				$q->where('event_id', $event->id);
			});

		return $this->applyScopes($attendees);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->columns($this->getColumns())
			->addAction(['width' => '10%'])
			->ajax('')
			->parameters([
//				'dom'        => 'Bfrtip',
				'dom'        =>
					"<'row'<'col-sm-8 text-left'B><'col-sm-4'f>>" .
					"<'row'<'col-sm-12'tr>>" .
					"<'row'<'col-sm-5'i><'col-sm-7'p>>",
				'scrollX'    => false,
				'responsive' => true,
				'buttons'    => [
					['extend' => 'print', 'className' => 'btn-sm'],
					['extend' => 'reset', 'className' => 'btn-sm'],
					['extend' => 'reload', 'className' => 'btn-sm'],
					[
						'extend'    => 'collection',
						'text'      => '<i class="fa fa-download"></i> Export',
						'className' => 'btn-sm',
						'buttons'   => [
							'csv',
							'excel',
							'pdf',
						],
					],
					['extend' => 'colvis', 'className' => 'btn-sm']
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns()
	{
		return [
			'user_id'            => ['name' => 'user_id', 'data' => 'user_id'],
			'surname'            => ['name' => 'surname', 'data' => 'surname', 'visible' => false],
			'first_name'         => ['name' => 'first_name', 'data' => 'first_name'],
			'gender'             => ['name' => 'gender', 'data' => 'gender', 'visible' => false],
			'country'            => ['name' => 'country.name', 'data' => 'country.name'],
			'company'            => ['name' => 'company', 'data' => 'company'],
			'business_sector_id' => ['name' => 'business_sector_id', 'data' => 'business_sector_id', 'visible' => false],
			'job_position'       => ['name' => 'job_position.title', 'data' => 'job_position.title'],
			'telephone'          => ['name' => 'telephone', 'data' => 'telephone', 'visible' => false],
			'email'              => ['name' => 'email', 'data' => 'email'],
			'address'            => ['name' => 'address', 'data' => 'address', 'visible' => false],
			'visa_needed'        => ['name' => 'visa_needed', 'data' => 'visa_needed', 'visible' => false]
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'attendees';
	}
}
