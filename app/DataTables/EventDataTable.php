<?php

namespace App\DataTables;

use App\Models\Event;
use Yajra\Datatables\Services\DataTable;

class EventDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax()
	{
		return $this->datatables
			->eloquent($this->query())
			->editColumn('attendees', function (Event $event) {
				return $event->registered_attendees->count();
			})
			->addColumn('action', 'backend.events.datatables_actions')
			->make(true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query()
	{
		$events = Event::with(['venue', 'registered_attendees']);

		return $this->applyScopes($events);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->columns($this->getColumns())
			->addAction(['width' => '15%'])
			->ajax('')
			->parameters([
				'dom'     => 'Bfrtip',
				'scrollX' => false,
				'buttons' => [
					'print',
					'reset',
					'reload',
					[
						'extend'  => 'collection',
						'text'    => '<i class="fa fa-download"></i> Export',
						'buttons' => [
							'csv',
							'excel',
							'pdf',
						],
					],
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns()
	{
		return [
			'title'       => ['name' => 'title', 'data' => 'title'],
			'description' => ['name' => 'description', 'data' => 'description', 'visible' => false],
			'content'     => ['name' => 'content', 'data' => 'content', 'visible' => false],
			'venue'       => ['name' => 'venue.name', 'data' => 'venue.name'],
			'preferences' => ['name' => 'preferences', 'data' => 'preferences', 'visible' => false],
			'attendees'   => ['name' => 'attendees', 'data' => 'attendees', 'class' => 'text-center'],
			'starts_at'   => ['name' => 'starts_at', 'data' => 'starts_at'],
			'ends_at'     => ['name' => 'ends_at', 'data' => 'ends_at']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'events';
	}
}
