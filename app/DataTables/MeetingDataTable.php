<?php

namespace App\DataTables;

use App\Models\Meeting;
use Yajra\Datatables\Services\DataTable;

class MeetingDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax()
	{
		return $this->datatables
			->eloquent($this->query())
			->editColumn('first_attendee_name', function (Meeting $meeting) {
//				dd($meeting->first_attendee->toArray());
				return $meeting->first_attendee->first_name . ' ' . $meeting->first_attendee->surname;
			})
			->editColumn('second_attendee_name', function (Meeting $meeting) {
//				dd($meeting->first_attendee->toArray());
				return $meeting->second_attendee->first_name . ' ' . $meeting->second_attendee->surname;
			})
			->addColumn('action', 'backend.meetings.datatables_actions')
			->make(true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query()
	{
		$meetings = Meeting::with(['first_attendee', 'second_attendee', 'event', 'status']);

		return $this->applyScopes($meetings);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->columns($this->getColumns())
			->addAction(['width' => '10%'])
			->ajax('')
			->parameters([
				'dom'     => 'Bfrtip',
				'scrollX' => false,
				'buttons' => [
					'print',
					'reset',
					'reload',
					[
						'extend'  => 'collection',
						'text'    => '<i class="fa fa-download"></i> Export',
						'buttons' => [
							'csv',
							'excel',
							'pdf',
						],
					],
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns()
	{
		return [
			'event_id'        => ['name' => 'event.title', 'data' => 'event.title'],
			'first_attendee'  => ['name' => 'first_attendee_name', 'data' => 'first_attendee_name'],
			'second_attendee' => ['name' => 'second_attendee_name', 'data' => 'second_attendee_name'],
			'starts_at'       => ['name' => 'starts_at', 'data' => 'starts_at'],
			'ends_at'         => ['name' => 'ends_at', 'data' => 'ends_at'],
			'status'          => ['name' => 'status.description', 'data' => 'status.description'],
			'information'     => ['name' => 'information', 'data' => 'information']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'meetings';
	}
}
