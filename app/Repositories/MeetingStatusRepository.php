<?php

namespace App\Repositories;

use App\Models\MeetingStatus;
use InfyOm\Generator\Common\BaseRepository;

class MeetingStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MeetingStatus::class;
    }
}
