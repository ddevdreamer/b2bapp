<?php

namespace App\Repositories;

use App\Models\BusinessSector;
use InfyOm\Generator\Common\BaseRepository;

class BusinessSectorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BusinessSector::class;
    }
}
