<?php

namespace App\Repositories;

use App\Models\Attendee;
use InfyOm\Generator\Common\BaseRepository;

class AttendeeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'surname',
        'first_name',
        'gender',
        'country_id',
        'company',
        'business_sector_id',
        'position_id',
        'telephone',
        'email',
        'address',
        'visa_needed'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Attendee::class;
    }
}
