<?php

namespace App\Repositories;

use App\Models\Meeting;
use InfyOm\Generator\Common\BaseRepository;

class MeetingRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'event_id',
		'first_attendee',
		'second_attendee',
		'starts_at',
		'ends_at',
		'meeting_status_id',
		'information'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return Meeting::class;
	}


	public function checkForMeetingConflict($event, $first_attendee, $second_attendee, $request)
	{
		$starts_at = $request->meeting_starts_at;
		$ends_at = $request->meeting_ends_at;
		$existing_meetings = $this->scopeQuery(function ($query) use ($request) {
			return $query->where(function ($query) use ($request) {
				$query->where('starts_at', '<=', $request->meeting_starts_at);
				$query->where('starts_at', '>=', $request->meeting_ends_at);
			})
				->orWhere('starts_at', '>=', $request->meeting_ends_at);
//			return $query->whereBetween('starts_at', [$request->meeting_start_at, $request->meeting_ends_At])
//				->orWhereBetween('ends_at', [$request->start_at, $request->ends_At]);
		})->all();

		dd($existing_meetings);

		return true;
	}
}
