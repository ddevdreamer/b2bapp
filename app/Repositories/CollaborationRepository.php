<?php

namespace App\Repositories;

use App\Models\Collaboration;
use InfyOm\Generator\Common\BaseRepository;

class CollaborationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Collaboration::class;
    }
}
