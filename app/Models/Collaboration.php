<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Collaboration
 * @package App\Models
 * @version February 7, 2017, 1:01 pm UTC
 */
class Collaboration extends Model
{
    use SoftDeletes;

    public $table = 'collaborations';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:20|min:2',
        'description' => 'required|max:1000'
    ];

    
}
