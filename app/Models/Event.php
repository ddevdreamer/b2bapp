<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Event
 * @package App\Models
 * @version February 9, 2017, 2:17 pm UTC
 */
class Event extends Model
{
	use SoftDeletes;

	public $table = 'events';


	protected $dates = ['starts_at', 'ends_at', 'deleted_at'];


	public $fillable = [
		'title',
		'description',
		'content',
		'venue_id',
		'preferences',
		'starts_at',
		'ends_at'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'title'       => 'string',
		'content'     => 'string',
		'venue_id'    => 'integer',
		'preferences' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'title'       => 'required',
		'content'     => 'required',
		'venue_id'    => 'required',
		'preferences' => 'nullable',
		'starts_at'   => 'required',
		'ends_at'     => 'required'
	];

	public function venue()
	{
		return $this->hasOne(Venue::class, 'id', 'venue_id');
	}

	public function picture()
	{
		return $this->hasMany(Picture::class, 'id', 'event_id');
	}

	public function registered_attendees()
	{
		return $this->belongsToMany(Attendee::class, 'event_attendee')
			->withTimestamps();
	}

}
