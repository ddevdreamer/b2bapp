<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Attendee
 * @package App\Models
 * @version February 12, 2017, 10:58 am UTC
 */
class Attendee extends Model
{
	use SoftDeletes;

	public $table = 'attendees';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'user_id',
		'surname',
		'first_name',
		'gender',
		'country_id',
		'company',
		'business_sector_id',
		'position_id',
		'telephone',
		'email',
		'address',
		'visa_needed'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'user_id'            => 'integer',
		'surname'            => 'string',
		'first_name'         => 'string',
		'gender'             => 'integer',
		'country_id'         => 'integer',
		'company'            => 'string',
		'business_sector_id' => 'integer',
		'position_id'        => 'integer',
		'telephone'          => 'string',
		'email'              => 'string',
		'address'            => 'string',
		'visa_needed'        => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
//        'user_id' => 'required',
		'surname'            => 'required',
		'first_name'         => 'required',
		'gender'             => 'required',
		'country_id'         => 'required',
		'company'            => 'required',
		'business_sector_id' => 'required',
		'position_id'        => 'required',
		'telephone'          => 'required',
		'email'              => 'required|email|unique:users,email|unique:attendees,email',
		'address'            => 'required',
		'visa_needed'        => 'required'
	];

	public function collaborations()
	{
		return $this->belongsToMany(Collaboration::class);
	}

	public function registered_events()
	{
		return $this->belongsToMany(Event::class, 'event_attendee')
//			->withPivot('company', 'working_in', 'name')
			->withTimestamps();
	}

	public function job_position()
	{
		return $this->hasOne(JobPosition::class, 'id', 'position_id');
	}

	public function country()
	{
		return $this->belongsTo(Country::class, 'country_id', 'id');
	}

	public function business_sector()
	{
		return $this->hasOne(BusinessSector::class, 'id', 'business_sector_id');
	}

}
