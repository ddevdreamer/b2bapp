<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Picture
 * @package App\Models
 * @version February 21, 2017, 12:17 pm UTC
 */
class Picture extends Model
{
	use SoftDeletes;

	public $table = 'pictures';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'name',
		'image_path',
		'featured',
		'event_id'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'name'       => 'string',
		'image_path' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'name'       => 'required',
		'image_path' => 'required'
	];

	public function event()
	{
		return $this->belongsTo(Event::class, 'event_id', 'id');
	}
}
