<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Meeting
 * @package App\Models
 * @version February 22, 2017, 11:38 am UTC
 */
class Meeting extends Model
{
	use SoftDeletes;

	public $table = 'meetings';


	protected $dates = ['starts_at', 'ends_at', 'deleted_at'];


	public $fillable = [
		'event_id',
		'first_attendee_id',
		'second_attendee_id',
		'starts_at',
		'ends_at',
		'meeting_status_id',
		'information'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'event_id'          => 'integer',
		'first_attendee_id'    => 'integer',
		'second_attendee_id'   => 'integer',
		'meeting_status_id' => 'integer',
		'information'       => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'event_id'           => 'required',
		'first_attendee_id'  => 'required',
		'second_attendee_id' => 'required',
		'starts_at'          => 'required',
		'ends_at'            => 'required',
		'meeting_status_id'  => 'required'
	];

	public function setStartsAtAttribute($value)
	{
		$this->attributes['starts_at'] = $value ? Carbon::createFromFormat('Y-m-d H:i', $value) : NULL;
	}

	public function setEndsAtAttribute($value)
	{
		$this->attributes['ends_at'] = $value ? Carbon::createFromFormat('Y-m-d H:i', $value) : NULL;
	}

	public function first_attendee()
	{
		return $this->hasOne(Attendee::class, 'id', 'first_attendee_id');
	}

	public function second_attendee()
	{
		return $this->hasOne(Attendee::class, 'id', 'second_attendee_id');
	}

	public function event()
	{
		return $this->hasOne(Event::class, 'id', 'event_id');
	}

	public function status()
	{
		return $this->hasOne(MeetingStatus::class, 'id', 'meeting_status_id');
	}

}
