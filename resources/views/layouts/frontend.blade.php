<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('/frontend/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"/>

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<div id="app">
    @include('frontend._includes.header')


    @yield('content')
    @include('frontend._includes.footer')
</div>

<!-- Scripts -->
<script src="{{ mix('/frontend/js/app.js') }}"></script>
{{--<script src="{{ url('/frontend/js/libraries.js') }}"></script>--}}
<script src="/js/vendor/sweetalert2.js"></script>
@stack('bottomJS')
@include('sweet::alert')
</body>
</html>
