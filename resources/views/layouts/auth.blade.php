<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="/images/apple-icon.png"/>
    <link rel="icon" type="image/png" href="/images/favicon.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Material Dashboard Pro by Creative Tim</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
    <!-- Bootstrap core CSS     -->
    <link href="{{ mix('/backend/css/app.css') }}" rel="stylesheet"/>
{{--<link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />--}}
<!--  Material Dashboard CSS    -->
{{--<link href="../../assets/css/material-dashboard.css" rel="stylesheet" />--}}
<!--  CSS for Demo Purpose, don't include it in your project     -->
{{--<link href="../../assets/css/demo.css" rel="stylesheet" />--}}
<!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"/>
</head>

<body>
<nav class="navbar navbar-primary navbar-transparent navbar-absolute">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/home') }}">{{ config('app.name') }}</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                {{--<li>--}}
                {{--<a href="../home">--}}
                {{--<i class="material-icons">dashboard</i> Dashboard--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--<li class="">--}}
                {{--<a href="/register">--}}
                {{--<i class="material-icons">person_add</i> Register--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--<li class=" active ">--}}
                    {{--<a href="/login">--}}
                        {{--<i class="material-icons">fingerprint</i> Login--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="">--}}
                {{--<a href="lock.html">--}}
                {{--<i class="material-icons">lock_open</i> Lock--}}
                {{--</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
</nav>
<div class="wrapper wrapper-full-page">
    <div class="full-page login-page" filter-color="black" data-image="/images/login.jpeg">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                @yield('content')
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                {{--<nav class="pull-left">--}}
                {{--<ul>--}}
                {{--<li>--}}
                {{--<a href="#">--}}
                {{--Home--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<a href="#">--}}
                {{--Company--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<a href="#">--}}
                {{--Portfolio--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<a href="#">--}}
                {{--Blog--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--</ul>--}}
                {{--</nav>--}}
                <p class="copyright pull-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    {{--<a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web--}}
                </p>
            </div>
        </footer>
    </div>
</div>
</body>
<!--   Core JS Files   -->
{{--<script src="{{ mix('/frontend/js/manifest.js') }}" type="text/javascript"></script>--}}
{{--<script src="{{ mix('/frontend/js/vendor.js') }}" type="text/javascript"></script>--}}
<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
<script src="{{ mix('/backend/js/app.js') }}" type="text/javascript"></script>
{{--<script src="../../assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>--}}
{{--<script src="../../assets/js/jquery-ui.min.js" type="text/javascript"></script>--}}
{{--<script src="../../assets/js/bootstrap.min.js" type="text/javascript"></script>--}}
{{--<script src="../../assets/js/material.min.js" type="text/javascript"></script>--}}
{{--<script src="../../assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>--}}
<!-- Forms Validations Plugin -->
{{--<script src="../../assets/js/jquery.validate.min.js"></script>--}}
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
{{--<script src="../../assets/js/moment.min.js"></script>--}}
<!--  Charts Plugin -->
{{--<script src="../../assets/js/chartist.min.js"></script>--}}
<!--  Plugin for the Wizard -->
{{--<script src="../../assets/js/jquery.bootstrap-wizard.js"></script>--}}
<!--  Notifications Plugin    -->
{{--<script src="../../assets/js/bootstrap-notify.js"></script>--}}
<!-- DateTimePicker Plugin -->
{{--<script src="../../assets/js/bootstrap-datetimepicker.js"></script>--}}
<!-- Vector Map plugin -->
{{--<script src="../../assets/js/jquery-jvectormap.js"></script>--}}
<!-- Sliders Plugin -->
{{--<script src="../../assets/js/nouislider.min.js"></script>--}}
<!--  Google Maps Plugin    -->
{{--<script src="https://maps.googleapis.com/maps/api/js"></script>--}}
<!-- Select Plugin -->
{{--<script src="../../assets/js/jquery.select-bootstrap.js"></script>--}}
<!--  DataTables.net Plugin    -->
{{--<script src="../../assets/js/jquery.datatables.js"></script>--}}
<!-- Sweet Alert 2 plugin -->
{{--<script src="../../assets/js/sweetalert2.js"></script>--}}
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
{{--<script src="../../assets/js/jasny-bootstrap.min.js"></script>--}}
<!--  Full Calendar Plugin    -->
{{--<script src="../../assets/js/fullcalendar.min.js"></script>--}}
<!-- TagsInput Plugin -->
{{--<script src="../../assets/js/jquery.tagsinput.js"></script>--}}
<!-- Material Dashboard javascript methods -->
{{--<script src="../../assets/js/material-dashboard.js"></script>--}}
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
{{--<script src="../../assets/js/demo.js"></script>--}}
<script type="text/javascript">
    $().ready(function () {
        $('select').select2();

        demo.checkFullPageBackgroundImage();

        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>
</html>
