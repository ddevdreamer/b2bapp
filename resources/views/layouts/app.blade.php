<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="/images/apple-icon.png"/>
    <link rel="icon" type="image/png" href="/images/favicon.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>@yield('title') | B2B Scheduling Platform</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
    <!-- Bootstrap core CSS     -->
    <link href="{{ mix('/backend/css/app.css') }}" rel="stylesheet"/>
{{--<link href="../assets/css/bootstrap.min.css" rel="stylesheet" />--}}
<!--  Material Dashboard CSS    -->
{{--<link href="../assets/css/material-dashboard.css" rel="stylesheet" />--}}
<!--  CSS for Demo Purpose, don't include it in your project     -->
{{--<link href="../assets/css/demo.css" rel="stylesheet" />--}}
<!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"/>
    @stack('topCSS')
    @yield('css')
</head>

<body>
<div class="wrapper">
    <div class="sidebar" data-active-color="rose" data-background-color="black" data-image="/images/sidebar-1.jpg">
        <!--
    Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
    Tip 2: you can also add an image using data-image tag
    Tip 3: you can change the color of the sidebar with data-background-color="white | black"
-->
        <div class="logo">
            <a href="{{ url('/') }}" class="simple-text">
                {{ config('app.name') }}
            </a>
        </div>
        <div class="logo logo-mini">
            <a href="{{ url('/') }}" class="simple-text">
                SP
            </a>
        </div>
        <div class="sidebar-wrapper">
            <div class="user">
                <div class="photo">
                    {{--<img src="/images/faces/avatar.jpg"/>--}}
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                        @if($signedIn) {{ $currentUser->name }} @endif
                        <b class="caret"></b>
                    </a>
                    <div class="collapse" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="#">My Profile</a>
                            </li>
                            <li>
                                <a href="#">Edit Profile</a>
                            </li>
                            <li>
                                <a href="#">Settings</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav">
                @include('layouts.menu')
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                        <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                        <i class="material-icons visible-on-sidebar-mini">view_list</i>
                    </button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Scheduling Platform</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="{{ url('/home') }}">
                                <i class="material-icons">dashboard</i>
                                <p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                        {{--<i class="material-icons">notifications</i>--}}
                        {{--<span class="notification">5</span>--}}
                        {{--<p class="hidden-lg hidden-md">--}}
                        {{--Notifications--}}
                        {{--<b class="caret"></b>--}}
                        {{--</p>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu">--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i>
                                <p class="hidden-lg hidden-md">Logout</p>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group form-search is-empty">
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="material-input"></span>
                        </div>
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                        </button>
                    </form>
                </div>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Company
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Portfolio
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Blog
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    Powered by <a href="http://mannai.tech">Mannai Tech</a>
                </p>
            </div>
        </footer>
    </div>
</div>
{{--@if (getenv('APP_ENV') === 'local')--}}
    {{--<script id="__bs_script__">//<![CDATA[--}}
        {{--document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.18.6'><\/script>".replace("HOST", location.hostname));--}}
        {{--//]]>--}}
    {{--</script>--}}
{{--@endif--}}
</body>
<!--   Core JS Files   -->
<script src="/js/vendor/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="/js/vendor/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/vendor/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/vendor/material.min.js" type="text/javascript"></script>
<script src="/js/vendor/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="/js/vendor/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="/js/vendor/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="/js/vendor/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="/js/vendor/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="/js/vendor/bootstrap-notify.js"></script>
<!-- DateTimePicker Plugin -->
<script src="/js/vendor/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="/js/vendor/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="/js/vendor/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
{{--<script src="https://maps.googleapis.com/maps/api/js"></script>--}}
<!-- Select Plugin -->
<script src="/js/vendor/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="/js/vendor/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="/js/vendor/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="/js/vendor/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="/js/vendor/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="/js/vendor/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="/js/vendor/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="/js/vendor/demo.js"></script>
@yield('scripts')
@stack('bottomJS')
@include('sweet::alert')

</html>
