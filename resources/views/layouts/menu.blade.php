<li class="{{ Request::is('home') ? 'active' : '' }}">
    <a href="{{ url('/home') }}" title="Dashboard">
        <i class="material-icons">dashboard</i>
        <p>Dashboard</p>
    </a>
</li>
@role('admin')
<li class="{{ Request::is('admin/attendees*') ? 'active' : '' }}">
    <a href="{!! route('admin.attendees.index') !!}" title="Attendees">
        <i class="material-icons">event_seat</i>
        <p>Attendees</p>
    </a>
</li>
<li class="{{ Request::is('admin/events*') ? 'active' : '' }}">
    <a href="{!! route('admin.events.index') !!}" title="Events">
        <i class="material-icons">event_available</i>
        <p>Events</p>
    </a>
    {{--<ul>--}}
    {{--<li class="{{ Request::is('pictures*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('admin.pictures.index') !!}"><i class="fa fa-edit"></i><span>Pictures</span></a>--}}
    {{--</li>--}}
    {{--</ul>--}}
</li>
<li class="{{ Request::is('meetings*') ? 'active' : '' }}">
    <a href="{!! route('admin.meetings.index') !!}">
        <i class="material-icons">device_hub</i>
        <p>Meetings</p>
    </a>
</li>


<li>
    <a data-toggle="collapse" href="#tablesExamples">
        <i class="material-icons">settings_applications</i>
        <p>Settings
            <b class="caret"></b>
        </p>
    </a>
    <div class="collapse" id="tablesExamples">
        <ul class="nav">

            <li class="{{ Request::is('admin/collaborations*') ? 'active' : '' }}">
                <a href="{!! route('admin.collaborations.index') !!}" title="Collaborations">
                    <i class="material-icons">toc</i>
                    <p>Collaborations</p>
                </a>
            </li>

            <li class="{{ Request::is('admin/venues*') ? 'active' : '' }}">
                <a href="{!! route('admin.venues.index') !!}" title="Venues">
                    <i class="material-icons">place</i>
                    <p>Venues</p>
                </a>
            </li>


            <li class="{{ Request::is('admin/jobPositions*') ? 'active' : '' }}">
                <a href="{!! route('admin.jobPositions.index') !!}" title="Job Positions">
                    <i class="material-icons">local_activity</i>
                    <p>Job Positions</p>
                </a>
            </li>

            <li class="{{ Request::is('admin/businessSectors*') ? 'active' : '' }}">
                <a href="{!! route('admin.businessSectors.index') !!}" title="Business Sectors">
                    <i class="material-icons">work</i>
                    <p>Business Sectors</p>
                </a>
            </li>

            <li class="{{ Request::is('meetingStatuses*') ? 'active' : '' }}">
                <a href="{!! route('admin.meetingStatuses.index') !!}"><i
                        class="fa fa-edit"></i><span>MeetingStatuses</span></a>
            </li>
        </ul>
    </div>
</li>
@endrole

@role('attendee')
@endrole


