@extends('layouts.auth')

@section('content')
    <div class="row">
        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
                <div class="card card-login">
                    <div class="card-header text-center" data-background-color="rose">
                        <h4 class="card-title">Login</h4>
                        {{--<div class="social-line">--}}
                        {{--<a href="#btn" class="btn btn-just-icon btn-simple">--}}
                        {{--<i class="fa fa-facebook-square"></i>--}}
                        {{--</a>--}}
                        {{--<a href="#pablo" class="btn btn-just-icon btn-simple">--}}
                        {{--<i class="fa fa-twitter"></i>--}}
                        {{--</a>--}}
                        {{--<a href="#eugen" class="btn btn-just-icon btn-simple">--}}
                        {{--<i class="fa fa-google-plus"></i>--}}
                        {{--</a>--}}
                        {{--</div>--}}
                    </div>
                    {{--<p class="category text-center">--}}
                    {{--Or Be Classical--}}
                    {{--</p>--}}
                    <div class="card-content">
                        @include('layouts._partials.errors')
                        {{--<div class="input-group">--}}
                        {{--<span class="input-group-addon">--}}
                        {{--<i class="material-icons">face</i>--}}
                        {{--</span>--}}
                        {{--<div class="form-group label-floating">--}}
                        {{--<label class="control-label">First Name</label>--}}
                        {{--<input type="text" class="form-control">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} label-floating">
                                <label class="control-label">Email</label>
                                <input id="email" type="text" class="form-control" name="email"
                                       value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                            <div class="form-group label-floating">
                                <label class="control-label">Password</label>
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"
                                               name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <button type="submit" class="btn btn-rose btn-simple btn-wd btn-lg">Login</button>
                        <br>
                        <a href="{{ url('/') }}" class="btn btn-simple">&larr; Back to Home</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-2 col-sm-offset-5">
            <div class="poweredByImage">
                <img src="/images/poweredby-logo.png" class="img img-responsive" alt="">
            </div>
        </div>
    </div>

@endsection

