@extends('layouts.auth')

@section('content')
    <div class="row">
        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
            <form method="post" action="{{ url('/register') }}">
                {!! csrf_field() !!}
                <div class="card card-login card-hidden">
                    <div class="card-header text-center" data-background-color="rose">
                        <h4 class="card-title">Register New Account</h4>
                        {{--<div class="social-line">--}}
                        {{--<a href="#btn" class="btn btn-just-icon btn-simple">--}}
                        {{--<i class="fa fa-facebook-square"></i>--}}
                        {{--</a>--}}
                        {{--<a href="#pablo" class="btn btn-just-icon btn-simple">--}}
                        {{--<i class="fa fa-twitter"></i>--}}
                        {{--</a>--}}
                        {{--<a href="#eugen" class="btn btn-just-icon btn-simple">--}}
                        {{--<i class="fa fa-google-plus"></i>--}}
                        {{--</a>--}}
                        {{--</div>--}}
                    </div>
                    {{--<p class="category text-center">--}}
                    {{--Or Be Classical--}}
                    {{--</p>--}}
                    <div class="card-content">

                        @include('layouts._partials.errors')
                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">contacts</i>
                                            </span>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} label-floating">
                                <label class="control-label">Full Name</label>
                                <input id="email" type="text" class="form-control" name="name"
                                       value="{{ old('name') }}" required>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} label-floating">
                                <label class="control-label">Email</label>
                                <input id="email" type="text" class="form-control" name="email"
                                       value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                            <div class="form-group label-floating">
                                <label class="control-label">Password</label>
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                            <div class="form-group label-floating">
                                <label class="control-label">Confirm Password</label>
                                <input id="password_confirmation" type="password" class="form-control"
                                       name="password_confirmation" required>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="checkbox icheck">
                                    <label>
                                        <input type="checkbox"> I agree to the <a href="#">terms</a>
                                    </label>
                                </div>
                            </div>
                            <!-- /.col -->
                        {{--<div class="col-xs-4">--}}
                        {{--<button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>--}}
                        {{--</div>--}}
                        <!-- /.col -->
                        </div>
                    </div>
                    <div class="footer text-center">
                        <button type="submit" class="btn btn-rose btn-simple btn-wd btn-lg">Let's go</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('content1')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
