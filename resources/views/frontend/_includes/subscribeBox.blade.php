<section class="subscribe_for_events">
    <div class="inner_shadow">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="emailIcon">
                            <span class="fa-stack fa-3x fa-lg">
                                <i class="fa  fa-circle-thin fa-stack-2x"></i>
                                <i class="fa  fa-envelope fa-stack-1x"></i>
                            </span>
                    </div>
                    <div class="clearfix"></div>
                    <h2>Subscribe for event updates</h2>
                    <div class="row">
                        <div class="input-group col-sm-6 col-sm-offset-3 text-center">
                            <input type="text" class="form-control input-lg"
                                   placeholder="Enter your email address here">
                            <span class="input-group-btn">
                                    <button type="button" class="btn btn-lg btn-primary">SUBSCRIBE</button>
                                </span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</section>