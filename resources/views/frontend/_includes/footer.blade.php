<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-12">
                                <img src="{{ url('/images/logo-footer.png') }}"
                                     class="img-responsive  center-block logoImg" alt="">
                            </div>
                            <div class="col-sm-12">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad ea explicabo praesentium
                                    repellat sed, sit voluptatum? Autem ipsum itaque laboriosam, maiores minus nisi odit
                                    quia
                                    quo temporibus tenetur unde voluptatibus.</p>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-4">
                        <h4>Useful Links</h4>
                        <div class="clearfix"></div>
                        <ul class="footer-links">
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li><a href="{{ url('/') }}">About Us</a></li>
                            <li><a href="{{ url('/') }}">Registration</a></li>
                            <li><a href="{{ url('/') }}">Blog</a></li>
                            <li><a href="{{ url('/') }}">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <h4>Latest News</h4>
                        <div class="newsItem">
                            <h5>Heading 1</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At consequatur dolore
                                dolores</p>
                        </div>
                        <div class="newsItem">
                            <h5>Heading 1</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At consequatur dolore
                                dolores</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <hr class="footerHr">
                    </div>
                    <div class="col-sm-6">
                        &copy; Copyright {{ \Carbon\Carbon::now()->year }}
                    </div>
                    <div class="col-sm-6 text-right social_links">
                        <i class="fa fa-2x fa-twitter-square"></i>
                        <i class="fa fa-2x  fa-facebook-square"></i>
                        <i class="fa fa-2x  fa-instagram"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
