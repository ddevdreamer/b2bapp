@extends('layouts.frontend')
@section('content')
    <section class="eventsHeader">
        <div class="inner_shadow">
            <h2>
                <span>Events</span>
            </h2>
            <p>Overview of all global events</p>

        </div>
    </section>

    <section class="events_details">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        @foreach($events as $event)
                            {{--<div class="grid-item">--}}
                            {{--<div class="event--single">--}}
                            <div class="col-md-4">
                                <div class="card card-product">
                                    <div class="card-image" data-header-animation="true">
                                        <a href="javascript:void(0);">
                                            {{--@if($event->image_path)--}}
                                            {{--<img src="{{ url($event->image_path) }}?w=450" class="img"--}}
                                            {{--alt="">--}}
                                            {{--@endif--}}
                                            <img src="/images/card-3.jpeg" class="img"
                                                 alt="">
                                        </a>
                                    </div>
                                    <div class="card-content">
                                        @role('admin')
                                        <div class="card-actions">
                                            <button type="button"
                                                    class="btn btn-danger btn-simple fix-broken-card">
                                                <i class="material-icons">build</i> Fix Header!
                                            </button>
                                            <button type="button" class="btn btn-default btn-simple"
                                                    rel="tooltip" data-placement="bottom" title="View">
                                                <i class="material-icons">art_track</i>
                                            </button>
                                            <button type="button" class="btn btn-success btn-simple"
                                                    rel="tooltip" data-placement="bottom" title="Edit">
                                                <i class="material-icons">edit</i>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-simple"
                                                    rel="tooltip" data-placement="bottom" title="Remove">
                                                <i class="material-icons">close</i>
                                            </button>
                                        </div>
                                        @endrole
                                        <h4 class="card-title">
                                            <a href="{{ route('frontend.event_detail',[$event->id]) }}">{{ $event->title }}</a>
                                        </h4>
                                        <div class="card-description">
                                            {{ $event->description }}
                                            {{--<div class="stats">--}}
                                            {{--<i class="material-icons">timer</i>--}}
                                            {{--<p>--}}
                                            {{--{{ $event->starts_at->toDayDateTimeString() }}--}}
                                            {{--- {{ $event->ends_at->toDayDateTimeString() }}--}}
                                            {{--</p>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats" data-toggle="tooltip" data-placement="top"
                                             title="Starts At: {{ $event->starts_at->toDayDateTimeString() }} - Ends At: {{ $event->ends_at->toDayDateTimeString() }}">
                                            <i class="material-icons">timer</i>
                                            {{ $event->starts_at->toDayDateTimeString() }}
                                        </div>
                                        <div class="stats pull-right">
                                            <i class="material-icons">place</i>
                                            {{ $event->venue->name }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--@if($event->image_path)--}}
                            {{--<img src="{{ url($event->image_path) }}?w=450" class="img-responsive"--}}
                            {{--alt="">--}}
                            {{--@endif--}}
                            {{--<div class="event--single-detail">--}}
                            {{--<span--}}
                            {{--class="event--single-date">{{ $event->starts_at->toDayDateTimeString() }}</span>--}}
                            {{--<h4>--}}
                            {{--<a href="{{ route('frontend.event_detail',[$event->id]) }}">{{ $event->title }}</a>--}}
                            {{--</h4>--}}
                            {{--<p>{{ $event->description }}</p>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        @endforeach
                    </div>
                </div>

                <div class="grid">
                    <!-- .grid-sizer empty element, only used for element sizing -->
                    <div class="grid-sizer"></div>
                    <div class="gutter-sizer"></div>


                </div>
            </div>
        </div>
        </div>
    </section>

    {{--    @include('frontend._partials.events')--}}
    {{--    @include('frontend._includes.subscribeBox')--}}
@endsection

@push('bottomJS')
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    //$('.grid').isotope({
    //itemSelector: '.grid-item',
    //percentPosition: true,
    //masonry: {
    //// use outer width of grid-sizer for columnWidth
    //columnWidth: '.grid-sizer',
    //gutter: '.gutter-sizer'
    //}
    //})
</script>
@endpush
