@extends('layouts.frontend')
@section('content')
    <div class="topImage">
        <img src="{{ asset('/images/frontend/header.jpg') }}" class="img-responsive" alt="">
    </div>

    @include('frontend._partials.events')
    @include('frontend._includes.subscribeBox')
@endsection
