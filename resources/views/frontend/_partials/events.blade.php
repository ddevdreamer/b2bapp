<section class="events__list__homepage">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="text-center">Latest Events</h2>
                <div class="table-responsive">
                    <div class="col-xs-3"> <!-- required for floating -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-left"><!-- 'tabs-right' for right tabs -->
                            <li class="active"><a href="#event1" data-toggle="tab">
                                    <div class="event_list_icon">
                                        <span class="event_list_date">01</span> <br>
                                        <span class="event_list_month_year">October, 2016</span>
                                    </div>
                                </a></li>
                            <li><a href="#event2" data-toggle="tab">
                                    <div class="event_list_icon">
                                        <span class="event_list_date">07</span> <br>
                                        <span class="event_list_month_year">October, 2016</span>
                                    </div>
                                </a></li>
                            <li><a href="#event3" data-toggle="tab">
                                    <div class="event_list_icon">
                                        <span class="event_list_date">09</span> <br>
                                        <span class="event_list_month_year">October, 2016</span>
                                    </div>
                                </a></li>
                            <li><a href="#event4" data-toggle="tab">
                                    <div class="event_list_icon">
                                        <span class="event_list_date">20</span> <br>
                                        <span class="event_list_month_year">October, 2016</span>
                                    </div>
                                </a></li>
                            <li><a href="#event5" data-toggle="tab">
                                    <div class="event_list_icon">
                                        <span class="event_list_date">21</span> <br>
                                        <span class="event_list_month_year">October, 2016</span>
                                    </div>
                                </a></li>
                        </ul>
                    </div>
                    <div class="col-xs-9">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="event1">
                                @foreach($recent_events as $event)
                                    <div class="event_item">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <div class="event_item_time">
                                                    <span>{{ $event->starts_at->format('g:i a') }}</span>,
                                                </div>
                                            </div>
                                            <div class="col-sm-10">
                                                <span>{{ $event->venue->name }}</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h4 class="text-left">{{ $event->title }}</h4>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>{{ $event->description }}</p>
                                            </div>
                                            <div class="col-sm-2">
                                                <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                     class="img-responsive img-circle" alt="">
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                            <div class="tab-pane" id="event2">
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>08:30 AM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>11:30 AM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>02:30 PM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>03:30 PM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="event3">
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>08:30 AM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>11:30 AM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>02:30 PM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>03:30 PM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="event4">
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>08:30 AM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>11:30 AM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>02:30 PM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>03:30 PM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="event5">
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>08:30 AM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>11:30 AM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>02:30 PM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="event_item">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="event_item_time">
                                                <span>03:30 PM</span>,
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>Manama, Bahrain</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h4 class="text-left">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </h4>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet
                                                animi assumenda
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url('/images/frontend/event_meeting_icon.jpg') }}"
                                                 class="img-responsive img-circle" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
