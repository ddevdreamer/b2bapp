@extends('layouts.frontend')
@section('content')
    <section class="registrationHeader">
        <div class="inner_shadow">
            <h2>
                <span>Registration</span>
            </h2>
            <p>Some text about events listing</p>

        </div>
    </section>
    <section class="registrationPage">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('frontend._partials.errors')
                    @include('vendor.flash.message')
                    <div class="row">
                        <div class="col-md-12 whiteBg">
                            <h4>Fill all the fields to complete registration</h4>
                            <hr>
                            <form action="{{ url('registration') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <!--- Surname Field --->
                                    <div
                                        class="form-group surname-field  has-feedback{{ $errors->has('surname') ? ' has-error' : '' }} col-sm-4">
                                        {!! Form::label('surname', 'Surname (as in passport):') !!}
                                        {!! Form::text('surname', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('surname', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <!--- First Name Field --->
                                    <div
                                        class="form-group  has-feedback{{ $errors->has('first_name') ? ' has-error' : '' }} col-sm-4">
                                        {!! Form::label('first_name', 'First Name:') !!}
                                        {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('first_name', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <!---Gender  Field --->
                                    <div
                                        class="form-group   has-feedback{{ $errors->has('gender') ? ' has-error' : '' }}  gender-field col-sm-4"
                                        id="gender-field">
                                        {!! Form::label('gender', 'Gender:') !!} <br>
                                        <label for="gender_male">
                                            {!! Form::radio('gender', '0', false, ['id'=>'gender_male']) !!} Male </label>
                                        <label for="gender_female">
                                            {!! Form::radio('gender', '1', false, ['id'=>'gender_female'])!!} Female </label>
                                        {!! $errors->first('gender', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <div class="clearfix"></div>
                                    <!--- Country Field --->
                                    <div
                                        class="form-group has-feedback{{ $errors->has('country_id') ? ' has-error' : '' }}  country-field col-sm-4"
                                        id="country-field">
                                        {!! Form::label('country_id', 'Country:') !!}
                                        {!! Form::select('country_id', Countries::getListForSelect(), null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('country_id', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <!--- Company Field --->
                                    <div
                                        class="form-group has-feedback{{ $errors->has('company') ? ' has-error' : '' }}  company-field col-sm-4"
                                        id="company-field">
                                        {!! Form::label('company', 'Institution / Company Name:') !!}
                                        {!! Form::text('company', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('company', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <!--- business_sector_id Field --->
                                    <div
                                        class="form-group has-feedback{{ $errors->has('business_sector_id') ? ' has-error' : '' }}  business_sector_id-field col-sm-4"
                                        id="business_sector_id-field">
                                        {!! Form::label('business_sector_id', 'Type of Current Business / Sector:') !!}
                                        {!! Form::select('business_sector_id', $businessSectors, null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('business_sector_id', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <!--- Job Position Field --->
                                    <div
                                        class="form-group has-feedback{{ $errors->has('position_id') ? ' has-error' : '' }}  position_id-field col-sm-4"
                                        id="position_id-field">
                                        {!! Form::label('position_id', 'Position / Official Job Title:') !!}
                                        {!! Form::select('position_id', $jobPositions, null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('position_id', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <!--- Telephone Field --->
                                    <div
                                        class="form-group  has-feedback{{ $errors->has('telephone') ? ' has-error' : '' }} telephone-field col-sm-4"
                                        id="telephone-field">
                                        {!! Form::label('telephone', 'Telephone:') !!}
                                        {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('telephone', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <!--- Email Field --->
                                    <div
                                        class="form-group  has-feedback{{ $errors->has('email') ? ' has-error' : '' }} email-field col-sm-4"
                                        id="email-field">
                                        {!! Form::label('email', 'Email:') !!}
                                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('email', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <!--- Address Field --->
                                    <div
                                        class="form-group has-feedback{{ $errors->has('address') ? ' has-error' : '' }}  address-field col-sm-4"
                                        id="address-field">
                                        {!! Form::label('address', 'Address:') !!}
                                        {!! Form::textarea('address', null, ['class' => 'form-control', 'rows'=>1]) !!}
                                        {!! $errors->first('address', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <p class="col-sm-12">What do you expect from the forum?</p>
                                    <!--- forum_agree Field --->
                                    <div
                                        class="form-group has-feedback{{ $errors->has('forum_agree') ? ' has-error' : '' }}  forum_agree-field col-sm-4"
                                        id="forum_agree-field">
                                        {!! Form::label('forum_agree', 'I will attend Bahrain Forum (Date): ') !!}
                                        <label for="forum_agree_yes">
                                            {!! Form::radio('forum_agree', '1', false, ['id'=>'forum_agree_yes']) !!}
                                            Yes </label>
                                        <label for="forum_agree_no">
                                            {!! Form::radio('forum_agree', '0', false, ['id'=>'forum_agree_no']) !!}
                                            No </label>
                                        {!! $errors->first('forum_agree', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <!--- visa_needed Field --->
                                    <div
                                        class="form-group  has-feedback{{ $errors->has('visa_needed') ? ' has-error' : '' }} visa_needed-field col-sm-6"
                                        id="visa_needed-field">
                                        {!! Form::label('visa_needed', 'Do you need visa support?') !!}
                                        <label for="visa_needed_yes">
                                            {!! Form::radio('visa_needed', '1', false, ['id'=>'visa_needed_yes']) !!}
                                            Yes </label>
                                        <label for="visa_needed_no">
                                            {!! Form::radio('visa_needed', '0', false, ['id'=>'visa_needed_no']) !!}
                                            No </label>
                                        {!! $errors->first('visa_needed', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    <p class="col-sm-12">If yes please send a copy of the first page of your passport to email
                                        address: </p>

                                    <!--- Collaboration sought Field --->
                                    <div
                                        class="form-group has-feedback{{ $errors->has('collaborationItems') ? ' has-error' : '' }}  collaboration-field col-sm-12"
                                        id="collaboration-field">
                                        {!! Form::label('collaboration_fields', 'Collaboration sought:') !!} <br>
                                        @foreach($collaborations as $collaboration => $value)
                                            <div class="col-sm-3">
                                                <label for="{{ $value }}">
                                                    {!! Form::checkbox('collaborationItems[]', $collaboration , false, ['class' => '', 'id'=>$value]) !!} {{ $value }}
                                                </label>
                                            </div>
                                        @endforeach
                                        <div class="clearfix"></div>
                                        {!! $errors->first('collaborationItems', '<p class="help-block error-msg">:message</p>') !!}
                                    </div>

                                    {{--<p class="col-sm-12">I would want meeting with </p>--}}

                                    {{--<!--- Meeting Company Field --->--}}
                                    {{--<div class="form-group meeting_company-field col-sm-4" id="meeting_company-field">--}}
                                    {{--{!! Form::label('meeting_company', 'Institution/Company:') !!}--}}
                                    {{--{!! Form::text('meeting_company', null, ['class' => 'form-control']) !!}--}}
                                    {{--{!! $errors->first('meeting_company', '<p class="help-block error-msg">:message</p>') !!}--}}
                                    {{--</div>--}}

                                    {{--<!--- Companies Working In Field --->--}}
                                    {{--<div class="form-group working_company-field col-sm-4" id="working_company-field">--}}
                                    {{--{!! Form::label('working_company', 'Companies Working In:') !!}--}}
                                    {{--{!! Form::text('working_company', null, ['class' => 'form-control']) !!}--}}
                                    {{--{!! $errors->first('working_company', '<p class="help-block error-msg">:message</p>') !!}--}}
                                    {{--</div>--}}

                                    {{--<!--- Meeting Name Field --->--}}
                                    {{--<div class="form-group meeting_name-field col-sm-4" id="meeting_name-field">--}}
                                    {{--{!! Form::label('meeting_name', 'Name (optional) :') !!}--}}
                                    {{--{!! Form::text('meeting_name', null, ['class' => 'form-control']) !!}--}}
                                    {{--{!! $errors->first('meeting_name', '<p class="help-block error-msg">:message</p>') !!}--}}
                                    {{--</div>--}}

                                    <div class="pull-right col-sm-4 text-right">
                                        <button class="btn btn-info">Register</button>
                                    </div>

                                    <div class="clearfix"></div>

                                </div>
                                <hr class="invisible">
                                <div class="clearfix"></div>

                            </form>
                        </div>
                        <hr class="invisible">
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
