@extends('layouts.frontend')
@section('content')
    <section class="eventsHeader">
        <div class="inner_shadow">
            <h2>
                <span>{{ $event->title }}</span>
            </h2>
        </div>
    </section>

    <section class="events_details">
        <div class="container">
            <div class="row">
                @if($event->image_path)
                    <div class="col-sm-6">
                        <img src="{{ url($event->image_path) }}" class="img-responsive img-thumbnail" alt="">
                    </div>
                @endif
                <div class="col-sm-6 event-detail--info">
                    <h4>{{ $event->title }} -
                        @if($signedIn && $currentUser->hasRole('attendee'))
                            @if($currentUser->attendee->registered_events->contains($event->id))
                                <a class='btn btn-success disabled btn-xs'>
                                    <i class="fa fa-check"></i> Registered
                                </a>
                            @else
                                <a href="{!! route('attendee.events_for_attendees.register', [$event->id]) !!}"
                                   class='btn btn-success btn-xs'><i
                                        class="fa fa-check"></i> Register</a>
                            @endif
                        @endif
                    </h4>
                    <hr>
                    @if($signedIn && $currentUser->hasRole('attendee') && $currentUser->attendee->registered_events->contains($event->id))
                        <form style="background: rgba(0,0,0,0.3); padding: 20px 0; border-radius: 5px;"
                              action="{{ route('attendee.events_for_attendees.registerInterest',[$event->id]) }}"
                              method="post">
                        {{ csrf_field() }}
                        <!--- Company Field --->
                            <div class="form-group company-field col-sm-6" id="company-field">
                                {!! Form::label('company', 'Company / Institute:') !!}
                                {!! Form::text('company', $eventInterestInfo['company'], ['class' => 'form-control']) !!}
                                {!! $errors->first('company', '<p class="help-block error-msg">:message</p>') !!}
                            </div>

                            <!--- Name Field --->
                            <div class="form-group name-field col-sm-6" id="name-field">
                                {!! Form::label('name', 'Name:') !!}
                                {!! Form::text('name', $eventInterestInfo['name'], ['class' => 'form-control']) !!}
                                {!! $errors->first('name', '<p class="help-block error-msg">:message</p>') !!}
                            </div>


                            <!--- WorkingIn Field --->
                            <div class="form-group working_in-field col-sm-12" id="working_in-field">
                                {!! Form::label('working_in', 'Working In:') !!}
                                {!! Form::textarea('working_in', $eventInterestInfo['working_in'], ['class' => 'form-control','rows'=>2]) !!}
                                {!! $errors->first('working_in', '<p class="help-block error-msg">:message</p>') !!}
                            </div>

                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                        <br>
                    @endif
                    <p>Starts : <span class="time_pill">{{ $event->starts_at->toDayDateTimeString() }}</span>, Ends:
                        <span
                            class="time_pill">{{ $event->ends_at->toDayDateTimeString() }}</span></p>
                    <p>Venue: {{ $event->venue->name }}</p>
                    <p>{{ $event->description }}</p>
                </div>
            </div>
        </div>
    </section>

    {{--    @include('frontend._partials.events')--}}
    {{--    @include('frontend._includes.subscribeBox')--}}
@endsection

@push('bottomJS')
@endpush
