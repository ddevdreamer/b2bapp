@extends('layouts.frontend')
@section('content')
    <section class="aboutUsHeader">
        <div class="inner_shadow">
            <h2>
                <span>Company Profile</span>
            </h2>
            <p>Some text about company profile</p>
        </div>
    </section>

    <section class="company_profile">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                                                                      data-toggle="tab">Who are we?</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                                       data-toggle="tab">Why choose us?</a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab"
                                                       data-toggle="tab">How to?</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <hr class="invisible">
                                <div class="row">
                                    <div class="col-sm-6 text-justify">
                                        <h3>Lorem ipsum dolor sit amet, consect adipiing elit </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="{{ url('/images/frontend/profile.jpg') }}" class="img-responsive" alt="">
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <hr class="invisible">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <img src="{{ url('/images/frontend/profile.jpg') }}" class="img-responsive" alt="">
                                    </div>
                                    <div class="col-sm-6 text-justify">
                                        <h3>Lorem ipsum dolor sit amet, consect adipiing elit </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                    </div>

                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="messages">
                                <hr class="invisible">
                                <div class="row">
                                    <div class="col-sm-6 text-justify">
                                        <h3>Lorem ipsum dolor sit amet, consect adipiing elit </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum fuga fugiat
                                            itaque nemo odio optio quasi tempora. Architecto aut consectetur delectus
                                            distinctio earum incidunt iure magnam nam quod similique. Placeat.
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="{{ url('/images/frontend/profile.jpg') }}" class="img-responsive" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--    @include('frontend._partials.events')--}}
    @include('frontend._includes.subscribeBox')
@endsection
