@extends('layouts.app')

@section('content')
    @role('admin')
    <div class="row">
        <div class="col-sm-12">
            @include('backend._widgets.admin.info_widgets')
        </div>
        <div class="col-sm-12">
            @include('backend._widgets.admin.country-chart')
        </div>
    </div>
    @endrole

    @role('attendee')

    @endrole
@endsection
