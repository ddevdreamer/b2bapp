@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Job Position
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($jobPosition, ['route' => ['admin.jobPositions.update', $jobPosition->id], 'method' => 'patch']) !!}

                        @include('backend.job_positions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection