@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Registered Attendees For Event</h1>
        <h1 class="pull-right">
            {{--           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('admin.attendees.create') !!}">Add New</a>--}}
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">{{ $event->name }}</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            @include('backend.event_registered_attendees.table')
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->

            </div>
        </div>
    </div>
@endsection

