@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Business Sector
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($businessSector, ['route' => ['admin.businessSectors.update', $businessSector->id], 'method' => 'patch']) !!}

                        @include('backend.business_sectors.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection