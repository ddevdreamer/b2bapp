{!! Form::open(['route' => ['admin.businessSectors.destroy', $id], 'method' => 'delete']) !!}
<a href="{{ route('admin.businessSectors.show', $id) }}" class="btn btn-simple btn-info btn-icon like"><i
        class="material-icons">favorite</i></a>
<a href="{{ route('admin.businessSectors.edit', $id) }}" class="btn btn-simple btn-warning btn-icon edit"><i
        class="material-icons">dvr</i></a>
{!! Form::button('<i class="material-icons">close</i>', [
    'type' => 'submit',
    'class' => 'btn btn-simple btn-danger btn-icon remove',
    'onclick' => "return confirm('Are you sure?')"
]) !!}
{!! Form::close() !!}
