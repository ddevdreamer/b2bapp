<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Venue Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('venue_id', 'Venue:') !!}
    {!! Form::select('venue_id', $venues, null, ['class' => 'form-control']) !!}
</div>


<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows'=> 2]) !!}
</div>


<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control', 'rows'=> 2]) !!}
</div>


<!-- Preferences Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('preferences', 'Preferences:') !!}
    {!! Form::textarea('preferences', null, ['class' => 'form-control', 'rows'=> 2]) !!}
</div>

<!-- Starts At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('starts_at', 'Starts At:') !!}
    {!! Form::text('starts_at', null, ['class' => 'form-control datetimepicker']) !!}
</div>

<!-- Ends At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ends_at', 'Ends At:') !!}
    {!! Form::text('ends_at', null, ['class' => 'form-control datetimepicker']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.events.index') !!}" class="btn btn-default">Cancel</a>
</div>

@push('topCSS')
{{--<link rel="stylesheet"--}}
      {{--href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css">--}}
@endpush
@push('bottomJS')
{{--<script type="text/javascript"--}}
        {{--src="https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.6.4/jquery.colorbox-min.js"></script>--}}
{{--<script type="text/javascript" src="/packages/barryvdh/elfinder/js/standalonepopup.min.js"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
{{--<script--}}
    {{--src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js">--}}
{{--</script>--}}
<script type="text/javascript">
    $(function () {
        $('#starts_at').datetimepicker({
            format: "YYYY-MM-DD HH:mm:ss",
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            {{--            defaultDate: "{{ \Carbon\Carbon::now()->format('m/d/Y') }}"--}}

        });
        $('#ends_at').datetimepicker({
            format: "YYYY-MM-DD HH:mm:ss",
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down",
                previous: "fa fa-arrow-left",
                next: "fa fa-arrow-right"

            },
            {{--            defaultDate: "{{ \Carbon\Carbon::now()->addDay()->format('m/d/Y') }}",--}}
            useCurrent: false //Important! See issue #1075
        });
        $("#starts_at").on("dp.change", function (e) {
            $('#ends_at').data("DateTimePicker").minDate(e.date);
        });
//        $("#starts_at").on("dp.change", function (e) {
//            $('#ends_at').data("DateTimePicker").maxDate(e.date);
//        });
    });
</script>
@endpush
