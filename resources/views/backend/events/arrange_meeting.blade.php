@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Meeting Arrangement</h1>
        <br>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="col-sm-12">
                    <div class="row">
                        <form
                            action="{{ route('admin.events.arrange_meeting.store',[$event->id,$first_attendee->id]) }}"
                            method="post">
                        {{ csrf_field() }}
                        <!--- First Attendee Field --->
                            <div class="form-group first_attendee-field col-sm-4" id="first_attendee-field">
                                {!! Form::label('first_attendee', 'First Attendee:') !!}
                                {!! Form::text('first_attendee', $first_attendee->first_name, ['class' => 'form-control','disabled'=>'disabled']) !!}
                                {!! $errors->first('first_attendee', '<p class="help-block error-msg">:message</p>') !!}
                            </div>
                            <!--- Event Field --->
                            <div class="form-group event-field col-sm-4" id="event-field">
                                {!! Form::label('event', 'Event') !!}
                                {!! Form::text('event', $event->title, ['class' => 'form-control','disabled'=>'disabled']) !!}
                                {!! $errors->first('event', '<p class="help-block error-msg">:message</p>') !!}
                            </div>

                            <!--- Venue Field --->
                            <div class="form-group venue-field col-sm-4" id="venue-field">
                                {!! Form::label('venue', 'Venue:') !!}
                                {!! Form::text('venue', $event->venue->name, ['class' => 'form-control','disabled'=>'disabled']) !!}
                                {!! $errors->first('venue', '<p class="help-block error-msg">:message</p>') !!}
                            </div>

                            <!--- Event Starts At Field --->
                            <div class="form-group event_starts_at-field col-sm-4" id="event_starts_at-field">
                                {!! Form::label('event_starts_at', 'Event Starts At:') !!}
                                {!! Form::text('event_starts_at', $event->starts_at->toDayDateTimeString(), ['class' => 'form-control','disabled'=>'disabled']) !!}
                                {!! $errors->first('event_starts_at', '<p class="help-block error-msg">:message</p>') !!}
                            </div>

                            <!--- Event Ends At Field --->
                            <div class="form-group event_ends_at-field col-sm-4" id="event_ends_at-field">
                                {!! Form::label('event_ends_at', 'Event Ends At:') !!}
                                {!! Form::text('event_ends_at', $event->ends_at->toDayDateTimeString(), ['class' => 'form-control','disabled'=>'disabled']) !!}
                                {!! $errors->first('event_ends_at', '<p class="help-block error-msg">:message</p>') !!}
                            </div>

                            <div class="clearfix"></div>

                            <!--- Second Attendee Field --->
                            <div class="form-group second_attendee-field col-sm-4" id="second_attendee-field">
                                {!! Form::label('second_attendee', 'Second Attendee:') !!}
                                {!! Form::select('second_attendee', $second_attendees, null, ['class' => 'form-control']) !!}
                                {!! $errors->first('second_attendee', '<p class="help-block error-msg">:message</p>') !!}
                            </div>

                            <!--- Meeting Starts At Field --->
                            <div class="form-group meeting_starts_at-field col-sm-4" id="meeting_starts_at-field">
                                {!! Form::label('meeting_starts_at', 'Meeting Starts At:') !!}
                                {!! Form::text('meeting_starts_at', null, ['class' => 'form-control datetimepicker','id'=>'meeting_starts_at']) !!}
                                {!! $errors->first('meeting_starts_at', '<p class="help-block error-msg">:message</p>') !!}
                            </div>

                            <!--- Meeting Ends At Field --->
                            <div class="form-group meeting_ends_at-field col-sm-4" id="meeting_ends_at-field">
                                {!! Form::label('meeting_ends_at', 'Meeting Ends At:') !!}
                                {!! Form::text('meeting_ends_at', null, ['class' => 'form-control datetimepicker','id'=>'meeting_ends_at']) !!}
                                {!! $errors->first('meeting_ends_at', '<p class="help-block error-msg">:message</p>') !!}
                            </div>

                            <div class="form-group col-sm-12">
                                <button class="btn btn-primary" type="submit">Submit</button>
                                <button class="btn btn-danger" type="reset">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{--@push('topCSS')--}}
{{--<link rel="stylesheet"--}}
      {{--href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css">--}}
{{--@endpush--}}
@push('bottomJS')
{{--<script type="text/javascript"--}}
        {{--src="https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.6.4/jquery.colorbox-min.js"></script>--}}
{{--<script type="text/javascript" src="/packages/barryvdh/elfinder/js/standalonepopup.min.js"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
{{--<script--}}
    {{--src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js">--}}
{{--</script>--}}
<script type="text/javascript">
    $(function () {
        $('#meeting_starts_at').datetimepicker({
            format: "YYYY-MM-DD HH:mm",
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            minDate: '{{ $event->starts_at }}',
            {{--            maxDate: '{{ $event->ends_at }}',--}}
            defaultDate: "{{ $event->starts_at }}"

        });
        $('#meeting_ends_at').datetimepicker({
            format: "YYYY-MM-DD HH:mm",
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down",
                previous: "fa fa-arrow-left",
                next: "fa fa-arrow-right"

            },
            minDate: '{{ $event->starts_at }}',
            maxDate: '{{ $event->ends_at }}',
            defaultDate: "{{ $event->ends_at }}",
            useCurrent: false //Important! See issue #1075
        });
        $("#meeting_starts_at").on("dp.change", function (e) {
            $('#meeting_ends_at').data("DateTimePicker").minDate(e.date);
        });
//        $("#starts_at").on("dp.change", function (e) {
//            $('#ends_at').data("DateTimePicker").maxDate(e.date);
//        });
    });
</script>
@endpush
