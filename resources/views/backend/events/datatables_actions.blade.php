{!! Form::open(['route' => ['admin.events.destroy', $id], 'method' => 'delete']) !!}
<a href='{{route('admin.events.registered_users',$id) }}'
   class="btn btn-simple btn-info btn-icon"
   title="Arrange Meeting"
><i
        class="material-icons">device_hub</i></a>
<a href="{{ route('admin.events.show', $id) }}"
   class="btn btn-simple btn-info btn-icon like"
   title="Show"
><i
        class="material-icons">visibility</i></a>
<a href="{{ route('admin.events.edit', $id) }}"
   class="btn btn-simple btn-warning btn-icon edit"
   title="Edit"
><i
        class="material-icons">dvr</i></a>
{!! Form::button('<i class="material-icons" title="Remove">close</i>', [
    'type' => 'submit',
    'class' => 'btn btn-simple btn-danger btn-icon remove',
    'onclick' => "return confirm('Are you sure?')"
]) !!}
{!! Form::close() !!}
