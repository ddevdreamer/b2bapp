@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Collaboration
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('backend.collaborations.show_fields')
                    <a href="{!! route('admin.collaborations.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
