@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Collaboration
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($collaboration, ['route' => ['admin.collaborations.update', $collaboration->id], 'method' => 'patch']) !!}

                        @include('backend.collaborations.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection