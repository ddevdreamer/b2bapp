@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Attendee
        </h1>
    </section>
    <div class="content">
        <div class="card">
            <div class="card-content">
                @include('backend.attendees.show_fields')
            </div>
        </div>
    </div>
@endsection
