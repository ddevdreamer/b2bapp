<!-- Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $attendee->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $attendee->user_id !!}</p>
</div>

<!-- Surname Field -->
<div class="form-group col-sm-4">
    {!! Form::label('surname', 'Surname:') !!}
    <p>{!! $attendee->surname !!}</p>
</div>

<!-- First Name Field -->
<div class="form-group col-sm-4">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{!! $attendee->first_name !!}</p>
</div>

<!-- Gender Field -->
<div class="form-group col-sm-4">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{!! $attendee->gender !!}</p>
</div>

<!-- Country Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('country_id', 'Country Id:') !!}
    <p>{!! $attendee->country_id !!}</p>
</div>

<!-- Company Field -->
<div class="form-group col-sm-4">
    {!! Form::label('company', 'Company:') !!}
    <p>{!! $attendee->company !!}</p>
</div>

<!-- Business Sector Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('business_sector_id', 'Business Sector Id:') !!}
    <p>{!! $attendee->business_sector_id !!}</p>
</div>

<!-- Position Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('position_id', 'Position Id:') !!}
    <p>{!! $attendee->position_id !!}</p>
</div>

<!-- Telephone Field -->
<div class="form-group col-sm-4">
    {!! Form::label('telephone', 'Telephone:') !!}
    <p>{!! $attendee->telephone !!}</p>
</div>

<!-- Email Field -->
<div class="form-group col-sm-4">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $attendee->email !!}</p>
</div>

<!-- Address Field -->
<div class="form-group col-sm-4">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $attendee->address !!}</p>
</div>

<!-- Visa Needed Field -->
<div class="form-group col-sm-4">
    {!! Form::label('visa_needed', 'Visa Needed:') !!}
    <p>{!! $attendee->visa_needed !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-sm-4">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $attendee->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-4">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $attendee->updated_at !!}</p>
</div>

