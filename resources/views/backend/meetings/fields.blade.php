<!-- Event Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_id', 'Event Id:') !!}
    {!! Form::text('event_id', null, ['class' => 'form-control']) !!}
</div>

<!-- First Attendee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_attendee_id', 'First Attendee:') !!}
    {!! Form::text('first_attendee_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Second Attendee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('second_attendee_id', 'Second Attendee:') !!}
    {!! Form::text('second_attendee_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Starts At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('starts_at', 'Starts At:') !!}
    {!! Form::text('starts_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Ends At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ends_at', 'Ends At:') !!}
    {!! Form::text('ends_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Meeting Status Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meeting_status_id', 'Meeting Status Id:') !!}
    {!! Form::text('meeting_status_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Information Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('information', 'Information:') !!}
    {!! Form::textarea('information', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.meetings.index') !!}" class="btn btn-default">Cancel</a>
</div>
