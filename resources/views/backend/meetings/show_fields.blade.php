<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $meeting->id !!}</p>
</div>

<!-- Event Id Field -->
<div class="form-group">
    {!! Form::label('event_id', 'Event Id:') !!}
    <p>{!! $meeting->event_id !!}</p>
</div>

<!-- First Attendee Field -->
<div class="form-group">
    {!! Form::label('first_attendee', 'First Attendee:') !!}
    <p>{!! $meeting->first_attendee !!}</p>
</div>

<!-- Second Attendee Field -->
<div class="form-group">
    {!! Form::label('second_attendee', 'Second Attendee:') !!}
    <p>{!! $meeting->second_attendee !!}</p>
</div>

<!-- Starts At Field -->
<div class="form-group">
    {!! Form::label('starts_at', 'Starts At:') !!}
    <p>{!! $meeting->starts_at !!}</p>
</div>

<!-- Ends At Field -->
<div class="form-group">
    {!! Form::label('ends_at', 'Ends At:') !!}
    <p>{!! $meeting->ends_at !!}</p>
</div>

<!-- Meeting Status Id Field -->
<div class="form-group">
    {!! Form::label('meeting_status_id', 'Meeting Status Id:') !!}
    <p>{!! $meeting->meeting_status_id !!}</p>
</div>

<!-- Information Field -->
<div class="form-group">
    {!! Form::label('information', 'Information:') !!}
    <p>{!! $meeting->information !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $meeting->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $meeting->updated_at !!}</p>
</div>

