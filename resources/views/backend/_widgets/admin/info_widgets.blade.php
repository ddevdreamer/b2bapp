<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="orange">
                <i class="material-icons">event_seat</i>
            </div>
            <div class="card-content">
                <p class="category">Attendees</p>
                <h3 class="card-title">{{ $registrations_by_country->sum('total') }}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-success">warning</i>
                    <a class="text-success" href="#pablo">List Attendees</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="rose">
                <i class="material-icons">event</i>
            </div>
            <div class="card-content">
                <p class="category">Events</p>
                <h3 class="card-title">{{ $events->count() }}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-success">warning</i>
                    <a class="text-success"
                       href="{{ route('admin.events.index') }}"
                    >List Events</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="orange">
                <i class="material-icons">device_hub</i>
            </div>
            <div class="card-content">
                <p class="category">Meetings</p>
                <h3 class="card-title">{{ $meetings->count() }}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-success">warning</i>
                    <a class="text-success"
                       href="{{ route('admin.meetings.index') }}"
                    >List Meetings</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="orange">
                <i class="material-icons">device_hub</i>
            </div>
            <div class="card-content">
                <p class="category">Countries</p>
                <h3 class="card-title">{{ $registrations_by_country->count() }}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-success">warning</i>
                    <a class="text-success" href="#pablo">List Meetings</a>
                </div>
            </div>
        </div>
    </div>
</div>

