<div class="card">
    <div class="card-header card-header-icon" data-background-color="green">
        <i class="material-icons">language</i>
    </div>
    <div class="card-content">
        <h4 class="card-title">Registration by Top Locations</h4>
        <div class="row">
            <div class="col-md-5">
                <div class="table-responsive registrations_by_country">
                    <table class="table country_content">
                        <tbody>
                        @foreach($registrations_by_country as $registrations)
                            <tr>
                                <td>
                                    <div class="flag">
                                        <img src="/images/flags/{{ $registrations->country->flag }}">
                                    </div>
                                </td>
                                <td>{{ $registrations->country->name }}</td>
                                <td class="text-right">
                                    {{ $registrations->total }}
                                </td>
                                <td class="text-right">
                                    {{ $registrations->percentage }}%
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6 col-md-offset-1">
                <div id="worldMap" class="map"></div>
            </div>
        </div>
    </div>
</div>

@push('bottomJS')
<script type="text/javascript">
    $(document).ready(function () {

        // Javascript method's body can be found in assets/js/demos.js
//        demo.initDashboardPageCharts();

//        demo.initVectorMap();
        var mapData = {!! $mapData !!}

        $('#worldMap').vectorMap({
            map: 'world_mill_en',
            backgroundColor: "transparent",
            zoomOnScroll: false,
            regionStyle: {
                initial: {
                    fill: '#e4e4e4',
                    "fill-opacity": 0.9,
                    stroke: 'none',
                    "stroke-width": 0,
                    "stroke-opacity": 0
                }
            },

            series: {
                regions: [{
                    values: mapData,
                    scale: ["#AAAAAA", "#444444"],
                    normalizeFunction: 'polynomial'
                }]
            },
        });
    });
</script>
@endpush
