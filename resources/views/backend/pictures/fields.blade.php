<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Path Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_path', 'Image Path:') !!}
    {!! Form::text('image_path', null, ['class' => 'form-control']) !!}
</div>

<!-- Featured Field -->
<div class="form-group col-sm-6">
    {!! Form::label('featured', 'Featured:') !!}
    <label class="checkbox-inline">
        {{--        {!! Form::hidden('featured', false) !!}--}}
        {!! Form::checkbox('featured', '1', null) !!} Yes
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.pictures.index',$event->id) !!}" class="btn btn-default">Cancel</a>
</div>
