<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MeetingStatusApiTest extends TestCase
{
    use MakeMeetingStatusTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMeetingStatus()
    {
        $meetingStatus = $this->fakeMeetingStatusData();
        $this->json('POST', '/api/v1/meetingStatuses', $meetingStatus);

        $this->assertApiResponse($meetingStatus);
    }

    /**
     * @test
     */
    public function testReadMeetingStatus()
    {
        $meetingStatus = $this->makeMeetingStatus();
        $this->json('GET', '/api/v1/meetingStatuses/'.$meetingStatus->id);

        $this->assertApiResponse($meetingStatus->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMeetingStatus()
    {
        $meetingStatus = $this->makeMeetingStatus();
        $editedMeetingStatus = $this->fakeMeetingStatusData();

        $this->json('PUT', '/api/v1/meetingStatuses/'.$meetingStatus->id, $editedMeetingStatus);

        $this->assertApiResponse($editedMeetingStatus);
    }

    /**
     * @test
     */
    public function testDeleteMeetingStatus()
    {
        $meetingStatus = $this->makeMeetingStatus();
        $this->json('DELETE', '/api/v1/meetingStatuses/'.$meetingStatus->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/meetingStatuses/'.$meetingStatus->id);

        $this->assertResponseStatus(404);
    }
}
