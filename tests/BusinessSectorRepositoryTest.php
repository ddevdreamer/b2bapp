<?php

use App\Models\BusinessSector;
use App\Repositories\BusinessSectorRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BusinessSectorRepositoryTest extends TestCase
{
    use MakeBusinessSectorTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BusinessSectorRepository
     */
    protected $businessSectorRepo;

    public function setUp()
    {
        parent::setUp();
        $this->businessSectorRepo = App::make(BusinessSectorRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBusinessSector()
    {
        $businessSector = $this->fakeBusinessSectorData();
        $createdBusinessSector = $this->businessSectorRepo->create($businessSector);
        $createdBusinessSector = $createdBusinessSector->toArray();
        $this->assertArrayHasKey('id', $createdBusinessSector);
        $this->assertNotNull($createdBusinessSector['id'], 'Created BusinessSector must have id specified');
        $this->assertNotNull(BusinessSector::find($createdBusinessSector['id']), 'BusinessSector with given id must be in DB');
        $this->assertModelData($businessSector, $createdBusinessSector);
    }

    /**
     * @test read
     */
    public function testReadBusinessSector()
    {
        $businessSector = $this->makeBusinessSector();
        $dbBusinessSector = $this->businessSectorRepo->find($businessSector->id);
        $dbBusinessSector = $dbBusinessSector->toArray();
        $this->assertModelData($businessSector->toArray(), $dbBusinessSector);
    }

    /**
     * @test update
     */
    public function testUpdateBusinessSector()
    {
        $businessSector = $this->makeBusinessSector();
        $fakeBusinessSector = $this->fakeBusinessSectorData();
        $updatedBusinessSector = $this->businessSectorRepo->update($fakeBusinessSector, $businessSector->id);
        $this->assertModelData($fakeBusinessSector, $updatedBusinessSector->toArray());
        $dbBusinessSector = $this->businessSectorRepo->find($businessSector->id);
        $this->assertModelData($fakeBusinessSector, $dbBusinessSector->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBusinessSector()
    {
        $businessSector = $this->makeBusinessSector();
        $resp = $this->businessSectorRepo->delete($businessSector->id);
        $this->assertTrue($resp);
        $this->assertNull(BusinessSector::find($businessSector->id), 'BusinessSector should not exist in DB');
    }
}
