<?php

use App\Models\Collaboration;
use App\Repositories\CollaborationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CollaborationRepositoryTest extends TestCase
{
    use MakeCollaborationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CollaborationRepository
     */
    protected $collaborationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->collaborationRepo = App::make(CollaborationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCollaboration()
    {
        $collaboration = $this->fakeCollaborationData();
        $createdCollaboration = $this->collaborationRepo->create($collaboration);
        $createdCollaboration = $createdCollaboration->toArray();
        $this->assertArrayHasKey('id', $createdCollaboration);
        $this->assertNotNull($createdCollaboration['id'], 'Created Collaboration must have id specified');
        $this->assertNotNull(Collaboration::find($createdCollaboration['id']), 'Collaboration with given id must be in DB');
        $this->assertModelData($collaboration, $createdCollaboration);
    }

    /**
     * @test read
     */
    public function testReadCollaboration()
    {
        $collaboration = $this->makeCollaboration();
        $dbCollaboration = $this->collaborationRepo->find($collaboration->id);
        $dbCollaboration = $dbCollaboration->toArray();
        $this->assertModelData($collaboration->toArray(), $dbCollaboration);
    }

    /**
     * @test update
     */
    public function testUpdateCollaboration()
    {
        $collaboration = $this->makeCollaboration();
        $fakeCollaboration = $this->fakeCollaborationData();
        $updatedCollaboration = $this->collaborationRepo->update($fakeCollaboration, $collaboration->id);
        $this->assertModelData($fakeCollaboration, $updatedCollaboration->toArray());
        $dbCollaboration = $this->collaborationRepo->find($collaboration->id);
        $this->assertModelData($fakeCollaboration, $dbCollaboration->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCollaboration()
    {
        $collaboration = $this->makeCollaboration();
        $resp = $this->collaborationRepo->delete($collaboration->id);
        $this->assertTrue($resp);
        $this->assertNull(Collaboration::find($collaboration->id), 'Collaboration should not exist in DB');
    }
}
