<?php

use Faker\Factory as Faker;
use App\Models\Venue;
use App\Repositories\VenueRepository;

trait MakeVenueTrait
{
    /**
     * Create fake instance of Venue and save it in database
     *
     * @param array $venueFields
     * @return Venue
     */
    public function makeVenue($venueFields = [])
    {
        /** @var VenueRepository $venueRepo */
        $venueRepo = App::make(VenueRepository::class);
        $theme = $this->fakeVenueData($venueFields);
        return $venueRepo->create($theme);
    }

    /**
     * Get fake instance of Venue
     *
     * @param array $venueFields
     * @return Venue
     */
    public function fakeVenue($venueFields = [])
    {
        return new Venue($this->fakeVenueData($venueFields));
    }

    /**
     * Get fake data of Venue
     *
     * @param array $postFields
     * @return array
     */
    public function fakeVenueData($venueFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $venueFields);
    }
}
