<?php

use Faker\Factory as Faker;
use App\Models\Meeting;
use App\Repositories\MeetingRepository;

trait MakeMeetingTrait
{
    /**
     * Create fake instance of Meeting and save it in database
     *
     * @param array $meetingFields
     * @return Meeting
     */
    public function makeMeeting($meetingFields = [])
    {
        /** @var MeetingRepository $meetingRepo */
        $meetingRepo = App::make(MeetingRepository::class);
        $theme = $this->fakeMeetingData($meetingFields);
        return $meetingRepo->create($theme);
    }

    /**
     * Get fake instance of Meeting
     *
     * @param array $meetingFields
     * @return Meeting
     */
    public function fakeMeeting($meetingFields = [])
    {
        return new Meeting($this->fakeMeetingData($meetingFields));
    }

    /**
     * Get fake data of Meeting
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMeetingData($meetingFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'event_id' => $fake->randomDigitNotNull,
            'first_attendee' => $fake->randomDigitNotNull,
            'second_attendee' => $fake->randomDigitNotNull,
            'starts_at' => $fake->date('Y-m-d H:i:s'),
            'ends_at' => $fake->date('Y-m-d H:i:s'),
            'meeting_status_id' => $fake->randomDigitNotNull,
            'information' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $meetingFields);
    }
}
