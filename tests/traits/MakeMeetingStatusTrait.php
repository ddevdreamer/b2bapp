<?php

use Faker\Factory as Faker;
use App\Models\MeetingStatus;
use App\Repositories\MeetingStatusRepository;

trait MakeMeetingStatusTrait
{
    /**
     * Create fake instance of MeetingStatus and save it in database
     *
     * @param array $meetingStatusFields
     * @return MeetingStatus
     */
    public function makeMeetingStatus($meetingStatusFields = [])
    {
        /** @var MeetingStatusRepository $meetingStatusRepo */
        $meetingStatusRepo = App::make(MeetingStatusRepository::class);
        $theme = $this->fakeMeetingStatusData($meetingStatusFields);
        return $meetingStatusRepo->create($theme);
    }

    /**
     * Get fake instance of MeetingStatus
     *
     * @param array $meetingStatusFields
     * @return MeetingStatus
     */
    public function fakeMeetingStatus($meetingStatusFields = [])
    {
        return new MeetingStatus($this->fakeMeetingStatusData($meetingStatusFields));
    }

    /**
     * Get fake data of MeetingStatus
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMeetingStatusData($meetingStatusFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $meetingStatusFields);
    }
}
