<?php

use Faker\Factory as Faker;
use App\Models\Attendee;
use App\Repositories\AttendeeRepository;

trait MakeAttendeeTrait
{
    /**
     * Create fake instance of Attendee and save it in database
     *
     * @param array $attendeeFields
     * @return Attendee
     */
    public function makeAttendee($attendeeFields = [])
    {
        /** @var AttendeeRepository $attendeeRepo */
        $attendeeRepo = App::make(AttendeeRepository::class);
        $theme = $this->fakeAttendeeData($attendeeFields);
        return $attendeeRepo->create($theme);
    }

    /**
     * Get fake instance of Attendee
     *
     * @param array $attendeeFields
     * @return Attendee
     */
    public function fakeAttendee($attendeeFields = [])
    {
        return new Attendee($this->fakeAttendeeData($attendeeFields));
    }

    /**
     * Get fake data of Attendee
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAttendeeData($attendeeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'surname' => $fake->word,
            'first_name' => $fake->word,
            'gender' => $fake->randomDigitNotNull,
            'country_id' => $fake->randomDigitNotNull,
            'company' => $fake->word,
            'business_sector_id' => $fake->randomDigitNotNull,
            'position_id' => $fake->randomDigitNotNull,
            'telephone' => $fake->word,
            'email' => $fake->word,
            'address' => $fake->text,
            'visa_needed' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $attendeeFields);
    }
}
