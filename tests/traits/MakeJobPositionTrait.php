<?php

use Faker\Factory as Faker;
use App\Models\JobPosition;
use App\Repositories\JobPositionRepository;

trait MakeJobPositionTrait
{
    /**
     * Create fake instance of JobPosition and save it in database
     *
     * @param array $jobPositionFields
     * @return JobPosition
     */
    public function makeJobPosition($jobPositionFields = [])
    {
        /** @var JobPositionRepository $jobPositionRepo */
        $jobPositionRepo = App::make(JobPositionRepository::class);
        $theme = $this->fakeJobPositionData($jobPositionFields);
        return $jobPositionRepo->create($theme);
    }

    /**
     * Get fake instance of JobPosition
     *
     * @param array $jobPositionFields
     * @return JobPosition
     */
    public function fakeJobPosition($jobPositionFields = [])
    {
        return new JobPosition($this->fakeJobPositionData($jobPositionFields));
    }

    /**
     * Get fake data of JobPosition
     *
     * @param array $postFields
     * @return array
     */
    public function fakeJobPositionData($jobPositionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'description' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $jobPositionFields);
    }
}
