<?php

use Faker\Factory as Faker;
use App\Models\BusinessSector;
use App\Repositories\BusinessSectorRepository;

trait MakeBusinessSectorTrait
{
    /**
     * Create fake instance of BusinessSector and save it in database
     *
     * @param array $businessSectorFields
     * @return BusinessSector
     */
    public function makeBusinessSector($businessSectorFields = [])
    {
        /** @var BusinessSectorRepository $businessSectorRepo */
        $businessSectorRepo = App::make(BusinessSectorRepository::class);
        $theme = $this->fakeBusinessSectorData($businessSectorFields);
        return $businessSectorRepo->create($theme);
    }

    /**
     * Get fake instance of BusinessSector
     *
     * @param array $businessSectorFields
     * @return BusinessSector
     */
    public function fakeBusinessSector($businessSectorFields = [])
    {
        return new BusinessSector($this->fakeBusinessSectorData($businessSectorFields));
    }

    /**
     * Get fake data of BusinessSector
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBusinessSectorData($businessSectorFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $businessSectorFields);
    }
}
