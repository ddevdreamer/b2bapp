<?php

use Faker\Factory as Faker;
use App\Models\Collaboration;
use App\Repositories\CollaborationRepository;

trait MakeCollaborationTrait
{
    /**
     * Create fake instance of Collaboration and save it in database
     *
     * @param array $collaborationFields
     * @return Collaboration
     */
    public function makeCollaboration($collaborationFields = [])
    {
        /** @var CollaborationRepository $collaborationRepo */
        $collaborationRepo = App::make(CollaborationRepository::class);
        $theme = $this->fakeCollaborationData($collaborationFields);
        return $collaborationRepo->create($theme);
    }

    /**
     * Get fake instance of Collaboration
     *
     * @param array $collaborationFields
     * @return Collaboration
     */
    public function fakeCollaboration($collaborationFields = [])
    {
        return new Collaboration($this->fakeCollaborationData($collaborationFields));
    }

    /**
     * Get fake data of Collaboration
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCollaborationData($collaborationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $collaborationFields);
    }
}
