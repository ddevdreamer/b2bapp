<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CollaborationApiTest extends TestCase
{
    use MakeCollaborationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCollaboration()
    {
        $collaboration = $this->fakeCollaborationData();
        $this->json('POST', '/api/v1/collaborations', $collaboration);

        $this->assertApiResponse($collaboration);
    }

    /**
     * @test
     */
    public function testReadCollaboration()
    {
        $collaboration = $this->makeCollaboration();
        $this->json('GET', '/api/v1/collaborations/'.$collaboration->id);

        $this->assertApiResponse($collaboration->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCollaboration()
    {
        $collaboration = $this->makeCollaboration();
        $editedCollaboration = $this->fakeCollaborationData();

        $this->json('PUT', '/api/v1/collaborations/'.$collaboration->id, $editedCollaboration);

        $this->assertApiResponse($editedCollaboration);
    }

    /**
     * @test
     */
    public function testDeleteCollaboration()
    {
        $collaboration = $this->makeCollaboration();
        $this->json('DELETE', '/api/v1/collaborations/'.$collaboration->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/collaborations/'.$collaboration->id);

        $this->assertResponseStatus(404);
    }
}
