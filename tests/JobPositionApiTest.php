<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class JobPositionApiTest extends TestCase
{
    use MakeJobPositionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateJobPosition()
    {
        $jobPosition = $this->fakeJobPositionData();
        $this->json('POST', '/api/v1/jobPositions', $jobPosition);

        $this->assertApiResponse($jobPosition);
    }

    /**
     * @test
     */
    public function testReadJobPosition()
    {
        $jobPosition = $this->makeJobPosition();
        $this->json('GET', '/api/v1/jobPositions/'.$jobPosition->id);

        $this->assertApiResponse($jobPosition->toArray());
    }

    /**
     * @test
     */
    public function testUpdateJobPosition()
    {
        $jobPosition = $this->makeJobPosition();
        $editedJobPosition = $this->fakeJobPositionData();

        $this->json('PUT', '/api/v1/jobPositions/'.$jobPosition->id, $editedJobPosition);

        $this->assertApiResponse($editedJobPosition);
    }

    /**
     * @test
     */
    public function testDeleteJobPosition()
    {
        $jobPosition = $this->makeJobPosition();
        $this->json('DELETE', '/api/v1/jobPositions/'.$jobPosition->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/jobPositions/'.$jobPosition->id);

        $this->assertResponseStatus(404);
    }
}
