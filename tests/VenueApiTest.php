<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VenueApiTest extends TestCase
{
    use MakeVenueTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateVenue()
    {
        $venue = $this->fakeVenueData();
        $this->json('POST', '/api/v1/venues', $venue);

        $this->assertApiResponse($venue);
    }

    /**
     * @test
     */
    public function testReadVenue()
    {
        $venue = $this->makeVenue();
        $this->json('GET', '/api/v1/venues/'.$venue->id);

        $this->assertApiResponse($venue->toArray());
    }

    /**
     * @test
     */
    public function testUpdateVenue()
    {
        $venue = $this->makeVenue();
        $editedVenue = $this->fakeVenueData();

        $this->json('PUT', '/api/v1/venues/'.$venue->id, $editedVenue);

        $this->assertApiResponse($editedVenue);
    }

    /**
     * @test
     */
    public function testDeleteVenue()
    {
        $venue = $this->makeVenue();
        $this->json('DELETE', '/api/v1/venues/'.$venue->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/venues/'.$venue->id);

        $this->assertResponseStatus(404);
    }
}
