<?php

use App\Models\Venue;
use App\Repositories\VenueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VenueRepositoryTest extends TestCase
{
    use MakeVenueTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var VenueRepository
     */
    protected $venueRepo;

    public function setUp()
    {
        parent::setUp();
        $this->venueRepo = App::make(VenueRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateVenue()
    {
        $venue = $this->fakeVenueData();
        $createdVenue = $this->venueRepo->create($venue);
        $createdVenue = $createdVenue->toArray();
        $this->assertArrayHasKey('id', $createdVenue);
        $this->assertNotNull($createdVenue['id'], 'Created Venue must have id specified');
        $this->assertNotNull(Venue::find($createdVenue['id']), 'Venue with given id must be in DB');
        $this->assertModelData($venue, $createdVenue);
    }

    /**
     * @test read
     */
    public function testReadVenue()
    {
        $venue = $this->makeVenue();
        $dbVenue = $this->venueRepo->find($venue->id);
        $dbVenue = $dbVenue->toArray();
        $this->assertModelData($venue->toArray(), $dbVenue);
    }

    /**
     * @test update
     */
    public function testUpdateVenue()
    {
        $venue = $this->makeVenue();
        $fakeVenue = $this->fakeVenueData();
        $updatedVenue = $this->venueRepo->update($fakeVenue, $venue->id);
        $this->assertModelData($fakeVenue, $updatedVenue->toArray());
        $dbVenue = $this->venueRepo->find($venue->id);
        $this->assertModelData($fakeVenue, $dbVenue->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteVenue()
    {
        $venue = $this->makeVenue();
        $resp = $this->venueRepo->delete($venue->id);
        $this->assertTrue($resp);
        $this->assertNull(Venue::find($venue->id), 'Venue should not exist in DB');
    }
}
