<?php

use App\Models\MeetingStatus;
use App\Repositories\MeetingStatusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MeetingStatusRepositoryTest extends TestCase
{
    use MakeMeetingStatusTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MeetingStatusRepository
     */
    protected $meetingStatusRepo;

    public function setUp()
    {
        parent::setUp();
        $this->meetingStatusRepo = App::make(MeetingStatusRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMeetingStatus()
    {
        $meetingStatus = $this->fakeMeetingStatusData();
        $createdMeetingStatus = $this->meetingStatusRepo->create($meetingStatus);
        $createdMeetingStatus = $createdMeetingStatus->toArray();
        $this->assertArrayHasKey('id', $createdMeetingStatus);
        $this->assertNotNull($createdMeetingStatus['id'], 'Created MeetingStatus must have id specified');
        $this->assertNotNull(MeetingStatus::find($createdMeetingStatus['id']), 'MeetingStatus with given id must be in DB');
        $this->assertModelData($meetingStatus, $createdMeetingStatus);
    }

    /**
     * @test read
     */
    public function testReadMeetingStatus()
    {
        $meetingStatus = $this->makeMeetingStatus();
        $dbMeetingStatus = $this->meetingStatusRepo->find($meetingStatus->id);
        $dbMeetingStatus = $dbMeetingStatus->toArray();
        $this->assertModelData($meetingStatus->toArray(), $dbMeetingStatus);
    }

    /**
     * @test update
     */
    public function testUpdateMeetingStatus()
    {
        $meetingStatus = $this->makeMeetingStatus();
        $fakeMeetingStatus = $this->fakeMeetingStatusData();
        $updatedMeetingStatus = $this->meetingStatusRepo->update($fakeMeetingStatus, $meetingStatus->id);
        $this->assertModelData($fakeMeetingStatus, $updatedMeetingStatus->toArray());
        $dbMeetingStatus = $this->meetingStatusRepo->find($meetingStatus->id);
        $this->assertModelData($fakeMeetingStatus, $dbMeetingStatus->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMeetingStatus()
    {
        $meetingStatus = $this->makeMeetingStatus();
        $resp = $this->meetingStatusRepo->delete($meetingStatus->id);
        $this->assertTrue($resp);
        $this->assertNull(MeetingStatus::find($meetingStatus->id), 'MeetingStatus should not exist in DB');
    }
}
