<?php

use App\Models\JobPosition;
use App\Repositories\JobPositionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class JobPositionRepositoryTest extends TestCase
{
    use MakeJobPositionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var JobPositionRepository
     */
    protected $jobPositionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->jobPositionRepo = App::make(JobPositionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateJobPosition()
    {
        $jobPosition = $this->fakeJobPositionData();
        $createdJobPosition = $this->jobPositionRepo->create($jobPosition);
        $createdJobPosition = $createdJobPosition->toArray();
        $this->assertArrayHasKey('id', $createdJobPosition);
        $this->assertNotNull($createdJobPosition['id'], 'Created JobPosition must have id specified');
        $this->assertNotNull(JobPosition::find($createdJobPosition['id']), 'JobPosition with given id must be in DB');
        $this->assertModelData($jobPosition, $createdJobPosition);
    }

    /**
     * @test read
     */
    public function testReadJobPosition()
    {
        $jobPosition = $this->makeJobPosition();
        $dbJobPosition = $this->jobPositionRepo->find($jobPosition->id);
        $dbJobPosition = $dbJobPosition->toArray();
        $this->assertModelData($jobPosition->toArray(), $dbJobPosition);
    }

    /**
     * @test update
     */
    public function testUpdateJobPosition()
    {
        $jobPosition = $this->makeJobPosition();
        $fakeJobPosition = $this->fakeJobPositionData();
        $updatedJobPosition = $this->jobPositionRepo->update($fakeJobPosition, $jobPosition->id);
        $this->assertModelData($fakeJobPosition, $updatedJobPosition->toArray());
        $dbJobPosition = $this->jobPositionRepo->find($jobPosition->id);
        $this->assertModelData($fakeJobPosition, $dbJobPosition->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteJobPosition()
    {
        $jobPosition = $this->makeJobPosition();
        $resp = $this->jobPositionRepo->delete($jobPosition->id);
        $this->assertTrue($resp);
        $this->assertNull(JobPosition::find($jobPosition->id), 'JobPosition should not exist in DB');
    }
}
