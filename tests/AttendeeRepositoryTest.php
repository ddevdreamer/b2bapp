<?php

use App\Models\Attendee;
use App\Repositories\AttendeeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AttendeeRepositoryTest extends TestCase
{
    use MakeAttendeeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AttendeeRepository
     */
    protected $attendeeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->attendeeRepo = App::make(AttendeeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAttendee()
    {
        $attendee = $this->fakeAttendeeData();
        $createdAttendee = $this->attendeeRepo->create($attendee);
        $createdAttendee = $createdAttendee->toArray();
        $this->assertArrayHasKey('id', $createdAttendee);
        $this->assertNotNull($createdAttendee['id'], 'Created Attendee must have id specified');
        $this->assertNotNull(Attendee::find($createdAttendee['id']), 'Attendee with given id must be in DB');
        $this->assertModelData($attendee, $createdAttendee);
    }

    /**
     * @test read
     */
    public function testReadAttendee()
    {
        $attendee = $this->makeAttendee();
        $dbAttendee = $this->attendeeRepo->find($attendee->id);
        $dbAttendee = $dbAttendee->toArray();
        $this->assertModelData($attendee->toArray(), $dbAttendee);
    }

    /**
     * @test update
     */
    public function testUpdateAttendee()
    {
        $attendee = $this->makeAttendee();
        $fakeAttendee = $this->fakeAttendeeData();
        $updatedAttendee = $this->attendeeRepo->update($fakeAttendee, $attendee->id);
        $this->assertModelData($fakeAttendee, $updatedAttendee->toArray());
        $dbAttendee = $this->attendeeRepo->find($attendee->id);
        $this->assertModelData($fakeAttendee, $dbAttendee->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAttendee()
    {
        $attendee = $this->makeAttendee();
        $resp = $this->attendeeRepo->delete($attendee->id);
        $this->assertTrue($resp);
        $this->assertNull(Attendee::find($attendee->id), 'Attendee should not exist in DB');
    }
}
