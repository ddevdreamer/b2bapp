<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BusinessSectorApiTest extends TestCase
{
    use MakeBusinessSectorTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBusinessSector()
    {
        $businessSector = $this->fakeBusinessSectorData();
        $this->json('POST', '/api/v1/businessSectors', $businessSector);

        $this->assertApiResponse($businessSector);
    }

    /**
     * @test
     */
    public function testReadBusinessSector()
    {
        $businessSector = $this->makeBusinessSector();
        $this->json('GET', '/api/v1/businessSectors/'.$businessSector->id);

        $this->assertApiResponse($businessSector->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBusinessSector()
    {
        $businessSector = $this->makeBusinessSector();
        $editedBusinessSector = $this->fakeBusinessSectorData();

        $this->json('PUT', '/api/v1/businessSectors/'.$businessSector->id, $editedBusinessSector);

        $this->assertApiResponse($editedBusinessSector);
    }

    /**
     * @test
     */
    public function testDeleteBusinessSector()
    {
        $businessSector = $this->makeBusinessSector();
        $this->json('DELETE', '/api/v1/businessSectors/'.$businessSector->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/businessSectors/'.$businessSector->id);

        $this->assertResponseStatus(404);
    }
}
