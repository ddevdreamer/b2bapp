<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AttendeeApiTest extends TestCase
{
    use MakeAttendeeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAttendee()
    {
        $attendee = $this->fakeAttendeeData();
        $this->json('POST', '/api/v1/attendees', $attendee);

        $this->assertApiResponse($attendee);
    }

    /**
     * @test
     */
    public function testReadAttendee()
    {
        $attendee = $this->makeAttendee();
        $this->json('GET', '/api/v1/attendees/'.$attendee->id);

        $this->assertApiResponse($attendee->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAttendee()
    {
        $attendee = $this->makeAttendee();
        $editedAttendee = $this->fakeAttendeeData();

        $this->json('PUT', '/api/v1/attendees/'.$attendee->id, $editedAttendee);

        $this->assertApiResponse($editedAttendee);
    }

    /**
     * @test
     */
    public function testDeleteAttendee()
    {
        $attendee = $this->makeAttendee();
        $this->json('DELETE', '/api/v1/attendees/'.$attendee->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/attendees/'.$attendee->id);

        $this->assertResponseStatus(404);
    }
}
