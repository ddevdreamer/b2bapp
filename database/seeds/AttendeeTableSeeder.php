<?php

use App\Models\Attendee;
use App\Models\Role;
use App\Models\User;
use App\Repositories\CollaborationRepository;
use Illuminate\Database\Seeder;

class AttendeeTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		foreach (range(1, 1000) as $i) {
			$user = factory(User::class)->create();

			/** @var Attendee $attendee */
			$attendee = factory(Attendee::class)->create([
				'user_id'    => $user->id,
				'first_name' => $user->name,
				'email'      => $user->email,
			]);

			$collaborationRepo = app()->make(CollaborationRepository::class);
			$collaborations = $collaborationRepo->all()->random(4)->pluck('id')->toArray();
//			var_dump($collaborations);
			$attendee->collaborations()->sync($collaborations);

			$attendeeRole = Role::where('name', 'attendee')->first();
			$user->attachRole($attendeeRole);
			var_dump($user->name);
		}
	}
}
