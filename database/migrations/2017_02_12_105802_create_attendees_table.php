<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAttendeesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attendees', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unique()->unsigned();
			$table->string('surname');
			$table->string('first_name');
			$table->integer('gender');
			$table->integer('country_id')->unsigned();
			$table->string('company');
			$table->integer('business_sector_id')->unsigned();
			$table->integer('position_id')->unsigned();
			$table->string('telephone');
			$table->string('email')->unique();
			$table->text('address');
			$table->integer('visa_needed');
			$table->timestamps();
			$table->softDeletes();
			$table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attendees');
	}
}
