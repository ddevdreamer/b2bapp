<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
	static $password;

	return [
		'name'           => $faker->firstName,
		'email'          => $faker->unique()->safeEmail,
		'password'       => $password ?: $password = bcrypt('secret'),
		'remember_token' => str_random(10),
	];
});


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Attendee::class, function (Faker\Generator $faker) {
	$businessSectorRepo = app()->make(\App\Repositories\BusinessSectorRepository::class);
	$business_sectors = $businessSectorRepo->all()->pluck('id')->toArray();

	$jobPositionsRepo = app()->make(\App\Repositories\JobPositionRepository::class);
	$jobPositions = $jobPositionsRepo->all()->pluck('id')->toArray();

	return [
//		'user_id'            => factory(\App\Models\User::class)->create()->id,
		'user_id'            => NULL,
		'surname'            => $faker->lastName,
		'first_name'         => $faker->firstName,
		'email'              => $faker->unique()->companyEmail,
		'gender'             => $faker->randomElement([0, 1]),
		'country_id'         => $faker->randomElement([48, 818, 784, 826, 840, 682, 634, 356, 156, 36, 124, 729]),
		'company'            => $faker->company,
		'business_sector_id' => $faker->randomElement($business_sectors),
		'position_id'        => $faker->randomElement($jobPositions),
		'telephone'          => $faker->phoneNumber,
		'address'            => $faker->address,
		'visa_needed'        => $faker->randomElement([0, 1])
	];
});


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Venue::class, function (Faker\Generator $faker) {
	$city = $faker->city;

	return [
		'name'        => $city,
		'description' => $city
	];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Event::class, function (Faker\Generator $faker) {
	$start_time = \Carbon\Carbon::instance($faker->dateTimeBetween($startDate = 'now', $endDate = '+ 20 days', $timezone = date_default_timezone_get()));
	$end_time = $start_time->copy()->addHours(4);

	$venuesRepo = app()->make(\App\Repositories\VenueRepository::class);
	$venues = $venuesRepo->all()->pluck('id')->toArray();

	return [
		'title'       => 'Event - ' . $faker->numberBetween(1111, 9999),
		'description' => $faker->sentences(1, true),
		'content'     => $faker->paragraphs(2, true),
		'venue_id'    => $faker->randomElement($venues),
		'starts_at'   => $start_time,
		'ends_at'     => $end_time,
	];
});
