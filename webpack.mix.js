const {mix} = require('laravel-mix').mix;
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/assets/frontend/js/app.js', 'public/frontend/js')
mix.js('resources/assets/backend/js/app.js', 'public/backend/js')
    .sass('resources/assets/frontend/sass/app.scss', 'public/frontend/css')
    .sass('resources/assets/backend/sass/app.scss', 'public/backend/css');

// mix.browserSync({
//     proxy: 'b2bapp.dev'
// });

// mix.extract(['vue', 'jquery']);

mix.webpackConfig({
    plugins: [
        new BrowserSyncPlugin({
            proxy: 'b2bapp.dev',
            files: [
                'app/**/*',
                'public/**/*',
                'resources/views/**/*',
                'routes/**/*'
            ]
        })
    ]
});